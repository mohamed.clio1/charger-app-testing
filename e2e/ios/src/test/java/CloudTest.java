import com.microsoft.appcenter.appium.Factory;
import com.microsoft.appcenter.appium.EnhancedIOSDriver;
import org.junit.rules.TestWatcher;
import org.junit.Rule;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.List;
import org.junit.Assert.*;
import org.junit.Assert;

import java.net.MalformedURLException;
import java.net.URL;

public class CloudTest {
    private EnhancedIOSDriver<IOSElement> driver;
    @Rule
    public TestWatcher watcher = Factory.createWatcher();

    public static EnhancedIOSDriver<IOSElement> startApp() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "ios");
        capabilities.setCapability("bundleId", "co.goe.charger-testing"); // For local tests: set your private bundleId in case you are not included in the development team
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
        capabilities.setCapability("deviceName", "iPhone 8");
        // capabilities.setCapability("platformVersion", "14.6"); // Not necessary for cloud testing; better use with local testing when you know exact version of your iOS
        capabilities.setCapability("udid", "auto");

        URL url = new URL("http://localhost:4723/wd/hub");

        return Factory.createIOSDriver(url, capabilities);
    }

    @Test
    public void homeScreenVisible() throws MalformedURLException, InterruptedException {
        driver = startApp();
        driver.label("App Launched");

        // 1a. Check if navigation button to Demo app is available
        List<IOSElement> navigateToDemoAppButtonElements = Util.getUniqueElemetList("Root_NavigateToNightOwlDemoAppButton", driver);
        System.out.println("Navigation buttons to Demo app elements count:");
        System.out.println(navigateToDemoAppButtonElements.size());
        driver.label("Navigation buttons to Demo app elements counted");
        System.out.println("------------");
        Assert.assertEquals(navigateToDemoAppButtonElements.size(), 1);

        // 1b. Push the button
        try {
            navigateToDemoAppButtonElements.get(0).click();
        } catch (Exception error) {
            error.printStackTrace();
        }

        // 1c. Check if navigation to Transport Demo button displayed
        Thread.sleep(1500);
        List<IOSElement> navigateToTransportDemoAppButtonElements = Util.getUniqueElemetList("DemoApp_NavigateToTransportSegmentButton", driver);
        System.out.println("Navigation buttons to Transport Demo elements count:");
        System.out.println(navigateToTransportDemoAppButtonElements.size());
        driver.label("Navigation buttons to Transport Demo app elements counted");
        System.out.println("------------");
        Assert.assertEquals(navigateToTransportDemoAppButtonElements.size(), 1);

        // 1d. Push the button
        try {
            navigateToTransportDemoAppButtonElements.get(0).click();
        } catch (Exception error) {
            error.printStackTrace();
        }

        // 1e. Deal with native notifications
        // This block will only run if there are native alerts and will stop after all of them are closed
        while (Util.getIsAlertVisible(driver)) {
            // If Bluetooth is switched off, an alert will appear that needs to be dismissed
            List<IOSElement> bluetoothIsOffAlertText = driver.findElements(By.xpath("//XCUIElementTypeStaticText[@name='Bluetooth is disabled']"));
            if (bluetoothIsOffAlertText.size() == 1) {
                driver.switchTo().alert().dismiss();
                System.out.println("Blutooth switch on alert: clicked on Cancel button");
            // Otherwise alerts are accepted
            } else {
                driver.switchTo().alert().accept();
            }
        }

        Thread.sleep(1000);
        // If Bluetooth switched off alert appears along with other alerts, Appium will not see it via getIsAlertVisible,
        // so we need to indicate it explicitly
        List<IOSElement> bluetoothIsOffAlertText = driver.findElements(By.xpath("//XCUIElementTypeStaticText[@name='Bluetooth is disabled']"));
        List<IOSElement> bluetoothIsOffAlertCancelButton = driver.findElements(By.xpath("//XCUIElementTypeButton[@name='Cancel']"));
        if (bluetoothIsOffAlertText.size() == 1 && bluetoothIsOffAlertCancelButton.size() == 1) {
            bluetoothIsOffAlertCancelButton.get(0).click();
            System.out.println("Blutooth switch on alert: clicked on Cancel button");
        }

        // 1f. Check if Home Screen is available
        Thread.sleep(1500);
        List<IOSElement> elements = Util.getUniqueElemetList("DemoApp_TransportSegment_HomeScreen", driver);
        System.out.println("Elements count:");
        System.out.println(elements.size());
        driver.label("Test elements counted");
        System.out.println("------------");
        Assert.assertEquals(elements.size(), 1);

        // 2. Test Base64Archive native library
        // 2.1 Test compress method
        List<IOSElement> base64ArchiveCompressButtonElements = Util.getUniqueElemetList("DemoApp_Base64ArchiveTestComponent_Compress_Button", driver);
        Assert.assertTrue(base64ArchiveCompressButtonElements.size() > 0);
        try {
            base64ArchiveCompressButtonElements.get(0).click();
        } catch (Exception error) {
            error.printStackTrace();
        }
        Thread.sleep(200);
        // Since we don't want to see unnesseray elements on the screen, the test value is added to the accessibilityId
        // If the accessibilityId of the button has been updated and includes the target value, the test is successful
        List<IOSElement> updatedBase64ArchiveCompressButtonElements = Util.getUniqueElemetList("DemoApp_Base64ArchiveTestComponent_Compress_Button H4sIAAAAAAAAE+PiDsjMSw9OLSrLTE4VYgFxpNi4WEpSi0sUWAGmTQHaHQAAAA==", driver);
        System.out.println("Updated base64 compress button elements count:");
        System.out.println(updatedBase64ArchiveCompressButtonElements.size());
        Assert.assertTrue(updatedBase64ArchiveCompressButtonElements.size() > 0);
        driver.label("Base64Archive compress method returned the target value");

        // 2.2 Test decompress method
        List<IOSElement> base64ArchiveDecompressButtonElements = Util.getUniqueElemetList("DemoApp_Base64ArchiveTestComponent_Decompress_Button", driver);
        Assert.assertTrue(base64ArchiveDecompressButtonElements.size() > 0);
        try {
            base64ArchiveDecompressButtonElements.get(0).click();
        } catch (Exception error) {
            error.printStackTrace();
        }
        Thread.sleep(200);
        // Since we don't want to see unnesseray elements on the screen, the test value is added to the accessibilityId
        // If the accessibilityId of the button has been updated and includes the target value, the test is successful
        List<IOSElement> updatedBase64ArchiveDecompressButtonElements = Util.getUniqueElemetList("DemoApp_Base64ArchiveTestComponent_Decompress_Button CgtQaW5nU2VydmljZRIEUGluZxoGCgR0ZXN0IAU=", driver);
        System.out.println("Updated base64 decompress button elements count:");
        System.out.println(updatedBase64ArchiveDecompressButtonElements.size());
        Assert.assertTrue(updatedBase64ArchiveDecompressButtonElements.size() > 0);
        driver.label("Base64Archive decompress method returned the target value");
    }

    @After
    public void after() throws Exception {
        if (driver != null) {
            driver.quit();
        }
    }

}
