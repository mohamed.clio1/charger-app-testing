import com.microsoft.appcenter.appium.EnhancedIOSDriver;
import io.appium.java_client.ios.IOSElement;
import com.google.common.collect.Lists;
import java.util.List;
import java.util.ArrayList;

// Returns true if a native alert is open
public class Util {
    public static boolean getIsAlertVisible(EnhancedIOSDriver<IOSElement> driver) {
        try {
            driver.switchTo().alert();
            return true;
        } catch (Exception error) {
            return false;
        }
    }

    /**
     * getUniqueElemetList
     * reduceListToUniqueElements
     * isSameElementLocation
     *
     * All three methods are designed to analyze element list and shed doubles if they are present
     *
     */
    public static boolean isSameElementLocation(IOSElement lastElementInList, IOSElement newElement) {
        return lastElementInList.getLocation().equals(newElement.getLocation());
    }

    public static List<IOSElement> reduceListToUniqueElements(List<IOSElement> resultList, IOSElement listElement) {
        if (resultList.size() == 0 || !isSameElementLocation(resultList.get(resultList.size() - 1), listElement)) {
            List<IOSElement> newList = new ArrayList();
            newList.addAll(resultList);
            newList.add(listElement);
            return newList;
        } else {
            return resultList;
        }
    }

    static List<IOSElement> emptyList = new ArrayList();
    public static List<IOSElement> getUniqueElemetList(String accessibilityId, EnhancedIOSDriver<IOSElement> driver) {
        List<IOSElement> elements = driver.findElementsByAccessibilityId(accessibilityId);

        if (elements.size() == 0 || elements.size() == 1) {
            return elements;
        } else {
            List<IOSElement> elementsReversed = Lists.reverse(elements);
            List<IOSElement> elementsFiltered = elementsReversed
                .stream()
                .reduce(
                    emptyList,
                    (resultList, listElement) -> reduceListToUniqueElements(resultList, listElement),
                    (resultList, intermediateLits) -> resultList
                );

            return elementsFiltered;
        }
    }
}