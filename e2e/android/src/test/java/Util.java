import com.microsoft.appcenter.appium.EnhancedAndroidDriver;
import io.appium.java_client.MobileElement;

// Returns true if a native alert is open
public class Util {
    public static boolean getIsAlertVisible(EnhancedAndroidDriver<MobileElement> driver) {
        try {
            driver.switchTo().alert();
            return true;
        } catch (Exception error) {
            return false;
        }
    }
}