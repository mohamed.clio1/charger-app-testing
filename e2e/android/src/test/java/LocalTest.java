import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.List;

import com.microsoft.appcenter.appium.Factory;
import com.microsoft.appcenter.appium.EnhancedAndroidDriver;
import org.junit.rules.TestWatcher;
import org.junit.Rule;
import org.junit.Assert;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class LocalTest {
    @Rule
    public TestWatcher watcher = Factory.createWatcher();

    private static EnhancedAndroidDriver<MobileElement> driver;

    public static EnhancedAndroidDriver<MobileElement> startApp() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "android");
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "foo");
//        capabilities.setCapability(MobileCapabilityType.APP, "[path to local repo]/AppCenter-Test-Samples/Appium/Android/swiftnotes.apk");
        // capabilities.setCapability("appPackage", "com.charger"); // only if the app is installed on the device and no metro bundler needed
        // capabilities.setCapability("appActivity", ".MainActivity"); // only if the app is installed on the device and no metro bundler needed
        // capabilities.setCapability("appWaitActivity", ".MainActivity"); // only if the app is installed on the device and no metro bundler needed
        capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 7913);
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiautomator2");

        URL url = new URL("http://localhost:4723/wd/hub");

        return Factory.createAndroidDriver(url, capabilities);
    }


    @Test
    public void localTest() throws MalformedURLException, InterruptedException {
        driver = startApp();
        ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        Thread.sleep(3000);

        // 1a. Check if navigation button to Demo app is available
        List<MobileElement> navigateToDemoAppButtonElements = driver.findElementsByAccessibilityId("Root_NavigateToNightOwlDemoAppButton");
        System.out.println("Navigation buttons to Demo app elements count:");
        System.out.println(navigateToDemoAppButtonElements.size());
        ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        System.out.println("------------");
        Assert.assertEquals(navigateToDemoAppButtonElements.size(), 1);

        // 1b. Push the button
        try {
            navigateToDemoAppButtonElements.get(0).click();
        } catch (Exception error) {
            error.printStackTrace();
        }

        // 1c. Check if navigation to Transport Demo button displayed
        Thread.sleep(1500);
        List<MobileElement> navigateToTransportDemoAppButtonElements = driver.findElementsByAccessibilityId("DemoApp_NavigateToTransportSegmentButton");
        System.out.println("Navigation buttons to Transport Demo elements count:");
        System.out.println(navigateToTransportDemoAppButtonElements.size());
        ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        System.out.println("------------");
        Assert.assertEquals(navigateToTransportDemoAppButtonElements.size(), 1);

        // 1d. Push the button
        try {
            navigateToTransportDemoAppButtonElements.get(0).click();
        } catch (Exception error) {
            error.printStackTrace();
        }

        // 1e. Deal with native notifications
        // This block will only run if there are native alerts and will stop after all of them are closed
        Thread.sleep(1500);
        while (Util.getIsAlertVisible(driver)) {
            List<MobileElement> bluetoothIsOffAlertText = driver.findElements(By.xpath("//android.widget.TextView[@text='Bluetooth is disabled']"));
            List<MobileElement> fineLocationPermissionAlertText = driver.findElements(By.xpath("//android.widget.TextView[contains(@text, 'Allow charger to access this')]"));
            // If Bluetooth is switched off, an alert will appear that needs to be dismissed
            if (bluetoothIsOffAlertText.size() == 1) {
                driver.switchTo().alert().dismiss();
                System.out.println("Blutooth switch on alert: clicked on Cancel button");
            // If Bluetooth is switched on, permission to use fine location will appear in alert (we should accept it)
            } else if (fineLocationPermissionAlertText.size() == 1) {
                driver.switchTo().alert().accept();
                System.out.println("Fine location permission alert: clicked on Allow button");
            }
        }

        // 1f. Check if Home Screen is available
        Thread.sleep(1500);
        List<MobileElement> homeScreenElements = driver.findElementsByAccessibilityId("DemoApp_TransportSegment_HomeScreen");
        System.out.println("Home Screen elements count:");
        System.out.println(homeScreenElements.size());
        ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        System.out.println("------------");
        Assert.assertEquals(homeScreenElements.size(), 1);

        // 2. Check if Charger devices present
        // Wait 5 seconds for chargers to connect with the device
        Thread.sleep(5000);
        // System.out.println(driver.getPageSource());
        // The xpath is based on app's XML structure. The structure can be retreived with driver.getPageSource() (the result should be printed).
        List<MobileElement> chargerListElements = driver.findElements(By.xpath("//android.view.ViewGroup[contains(@content-desc, 'DemoApp_TransportSegment_ChargerListItem_')]"));
        System.out.println("Charger list elements count:");
        System.out.println(chargerListElements.size());
        ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        System.out.println("------------");
        Assert.assertTrue(chargerListElements.size() > 0);

        // 3. Subtest: navigation to each device
        testChargerNavigation(chargerListElements, new ArrayList(), driver);

        // 4. Test Base64Archive native library

        // 4.1 Test compress method
        List<MobileElement> base64ArchiveCompressButtonElements = driver.findElementsByAccessibilityId("DemoApp_Base64ArchiveTestComponent_Compress_Button");
        Assert.assertTrue(base64ArchiveCompressButtonElements.size() > 0);
        try {
            base64ArchiveCompressButtonElements.get(0).click();
        } catch (Exception error) {
            error.printStackTrace();
        }
        Thread.sleep(200);
        // Since we don't want to see unnesseray elements on the screen, the test value is added to the accessibilityId
        // If the accessibilityId of the button has been updated and includes the target value, the test is successful
        List<MobileElement> updatedBase64ArchiveCompressButtonElements = driver.findElementsByAccessibilityId("DemoApp_Base64ArchiveTestComponent_Compress_Button eJzj4g7IzEsPTi0qy0xOFWIBcaTYuFhKUotLFFgBgIgILA==");
        System.out.println("Updated base64 compress button elements count:");
        System.out.println(updatedBase64ArchiveCompressButtonElements.size());
        Assert.assertTrue(updatedBase64ArchiveCompressButtonElements.size() > 0);
        System.out.println("Base64Archive compress method returned the target value");

        // 4.2 Test decompress method
        List<MobileElement> base64ArchiveDecompressButtonElements = driver.findElementsByAccessibilityId("DemoApp_Base64ArchiveTestComponent_Decompress_Button");
        Assert.assertTrue(base64ArchiveDecompressButtonElements.size() > 0);
        try {
            base64ArchiveDecompressButtonElements.get(0).click();
        } catch (Exception error) {
            error.printStackTrace();
        }
        Thread.sleep(200);
        // Since we don't want to see unnesseray elements on the screen, the test value is added to the accessibilityId
        // If the accessibilityId of the button has been updated and includes the target value, the test is successful
        List<MobileElement> updatedBase64ArchiveDecompressButtonElements = driver.findElementsByAccessibilityId("DemoApp_Base64ArchiveTestComponent_Decompress_Button CgtQaW5nU2VydmljZRIEUGluZxoGCgR0ZXN0IAU=");
        System.out.println("Updated base64 decompress button elements count:");
        System.out.println(updatedBase64ArchiveDecompressButtonElements.size());
        Assert.assertTrue(updatedBase64ArchiveDecompressButtonElements.size() > 0);
        System.out.println("Base64Archive decompress method returned the target value");
    }

    public void testChargerNavigation(List<MobileElement> chargerListElements, List<String> visitedChargerSerialNumberList, EnhancedAndroidDriver<MobileElement> driver) {
        try {
            // 3.1 Find chargers that has not been visited
            List<MobileElement> unvisitedChargerListElements = chargerListElements
                .stream()
                .filter(chargerListElement -> !visitedChargerSerialNumberList.contains(chargerListElement.getAttribute("content-desc")))
                .collect(Collectors.toList());

            // 3.1.1 If no chargers found the test is complete
            if (unvisitedChargerListElements.size() == 0) {
                System.out.println("All chargers tested. Finishing the test");
                return;
            }

            // 3.2 Navigate to the first of unvisited charger devices
            String currentChargerID = unvisitedChargerListElements.get(0).getAttribute("content-desc");
            try {
                unvisitedChargerListElements.get(0).click();
            } catch (Exception error) {
                error.printStackTrace();
            }

            // 3.3 Check if navigation lead to Charger Screen
            Thread.sleep(1000);
            List<MobileElement> chargerScreenElements = driver.findElementsByAccessibilityId("DemoApp_TransportSection_ChargerScreen");
            System.out.println("Charger " + currentChargerID + " elements count:");
            System.out.println(chargerScreenElements.size());
            ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            System.out.println("------------");
            Assert.assertEquals(chargerScreenElements.size(), 1); 

            // 3.4 Locate navigate back button
            List<MobileElement> navigateBackElements = driver.findElementsByAccessibilityId("DemoApp_TransportSegment_ChargerScreen_NavigateBackButton");
            System.out.println("Charger Screen navigate back button elements count:");
            System.out.println(navigateBackElements.size());
            ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            System.out.println("------------");
            Assert.assertTrue(navigateBackElements.size() > 0);

            // 3.5 Tap navigate back button
            navigateBackElements.get(0).click();

            // 3.6 Check that navigation back did take place
            Thread.sleep(1000);
            List<MobileElement> homeScreenElements = driver.findElementsByAccessibilityId("DemoApp_TransportSegment_HomeScreen");
            System.out.println("Home Screen elements count:");
            System.out.println(homeScreenElements.size());
            ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            System.out.println("------------");
            Assert.assertEquals(homeScreenElements.size(), 1);

            // 3.7 Renew list of visited chargers
            List<String> copyVisitedChargerSerialNumberList = new ArrayList();
            copyVisitedChargerSerialNumberList.addAll(visitedChargerSerialNumberList);
            copyVisitedChargerSerialNumberList.add(currentChargerID);
            System.out.println("Visited chargers IDs: " + copyVisitedChargerSerialNumberList);

            // 3.8 Renew list of displayed chargers
            List<MobileElement> newChargerListElements = driver.findElements(By.xpath("//android.view.ViewGroup[contains(@content-desc, 'DemoApp_TransportSegment_ChargerListItem_')]"));
            System.out.println("Charger list elements count:");
            System.out.println(newChargerListElements.size());
            ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            System.out.println("------------");
            Assert.assertTrue(newChargerListElements.size() > 0);

            // 3.9 Recursively run subtest 3 until all chargers visited
            testChargerNavigation(newChargerListElements, copyVisitedChargerSerialNumberList, driver);

        } catch (Exception error) {
            error.printStackTrace();
        }
    }

   @After
   public void after() throws Exception {
        if (driver != null) {
            ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            System.out.println("Stopping App");
            driver.quit();
        }
   }
}
