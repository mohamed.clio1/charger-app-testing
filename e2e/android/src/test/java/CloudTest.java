import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.List;

import com.microsoft.appcenter.appium.Factory;
import com.microsoft.appcenter.appium.EnhancedAndroidDriver;
import org.junit.rules.TestWatcher;
import org.junit.Rule;
import org.junit.Assert;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class CloudTest {
    @Rule
    public TestWatcher watcher = Factory.createWatcher();

    private static EnhancedAndroidDriver<MobileElement> driver;

    public static EnhancedAndroidDriver<MobileElement> startApp() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "android");
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "foo");
        capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 7913);
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiautomator2");

        URL url = new URL("http://localhost:4723/wd/hub");

        return Factory.createAndroidDriver(url, capabilities);
    }


    @Test
    public void homeScreenVisible() throws MalformedURLException, InterruptedException {
        driver = startApp();
        Thread.sleep(3000);

        // 1a. Check if navigation button to Demo app is available
        List<MobileElement> navigateToDemoAppButtonElements = driver.findElementsByAccessibilityId("Root_NavigateToNightOwlDemoAppButton");
        System.out.println("Navigation buttons to Demo app elements count:");
        System.out.println(navigateToDemoAppButtonElements.size());
        ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        System.out.println("------------");
        Assert.assertEquals(navigateToDemoAppButtonElements.size(), 1);

        // 1b. Push the button
        try {
            navigateToDemoAppButtonElements.get(0).click();
        } catch (Exception error) {
            error.printStackTrace();
        }

        // 1c. Check if navigation to Transport Demo button displayed
        Thread.sleep(1500);
        List<MobileElement> navigateToTransportDemoAppButtonElements = driver.findElementsByAccessibilityId("DemoApp_NavigateToTransportSegmentButton");
        System.out.println("Navigation buttons to Transport Demo elements count:");
        System.out.println(navigateToTransportDemoAppButtonElements.size());
        ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        System.out.println("------------");
        Assert.assertEquals(navigateToTransportDemoAppButtonElements.size(), 1);

        // 1d. Push the button
        try {
            navigateToTransportDemoAppButtonElements.get(0).click();
        } catch (Exception error) {
            error.printStackTrace();
        }

        // 1e. Deal with native notifications
        // This block will only run if there are native alerts and will stop after all of them are closed
        Thread.sleep(1500);
        while (Util.getIsAlertVisible(driver)) {
            List<MobileElement> bluetoothIsOffAlertText = driver.findElements(By.xpath("//android.widget.TextView[@text='Bluetooth is disabled']"));
            List<MobileElement> fineLocationPermissionAlertText = driver.findElements(By.xpath("//android.widget.TextView[contains(@text, 'Allow charger to access this')]"));
            // If Bluetooth is switched off, an alert will appear that needs to be dismissed
            if (bluetoothIsOffAlertText.size() == 1) {
                driver.switchTo().alert().dismiss();
                System.out.println("Blutooth switch on alert: clicked on Cancel button");
            // If Bluetooth is switched on, permission to use fine location will appear in alert (we should accept it)
            } else if (fineLocationPermissionAlertText.size() == 1) {
                driver.switchTo().alert().accept();
                System.out.println("Fine location permission alert: clicked on Allow button");
            }
        }

        // 1f. Check if Home Screen is available
        Thread.sleep(1500);
        List<MobileElement> elements = driver.findElementsByAccessibilityId("DemoApp_TransportSegment_HomeScreen");
        ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        System.out.println("Elements count:");
        System.out.println(elements.size());
        System.out.println("------------");
        Assert.assertEquals(elements.size(), 1);

        // 2. Test Base64Archive native library

        // 2.1 Test compress method
        List<MobileElement> base64ArchiveCompressButtonElements = driver.findElementsByAccessibilityId("DemoApp_Base64ArchiveTestComponent_Compress_Button");
        Assert.assertTrue(base64ArchiveCompressButtonElements.size() > 0);
        try {
            base64ArchiveCompressButtonElements.get(0).click();
        } catch (Exception error) {
            error.printStackTrace();
        }
        Thread.sleep(200);
        // Since we don't want to see unnesseray elements on the screen, the test value is added to the accessibilityId
        // If the accessibilityId of the button has been updated and includes the target value, the test is successful
        List<MobileElement> updatedBase64ArchiveCompressButtonElements = driver.findElementsByAccessibilityId("DemoApp_Base64ArchiveTestComponent_Compress_Button eJzj4g7IzEsPTi0qy0xOFWIBcaTYuFhKUotLFFgBgIgILA==");
        System.out.println("Updated base64 compress button elements count:");
        System.out.println(updatedBase64ArchiveCompressButtonElements.size());
        Assert.assertTrue(updatedBase64ArchiveCompressButtonElements.size() > 0);
        System.out.println("Base64Archive compress method returned the target value");

        // 2.2 Test decompress method
        List<MobileElement> base64ArchiveDecompressButtonElements = driver.findElementsByAccessibilityId("DemoApp_Base64ArchiveTestComponent_Decompress_Button");
        Assert.assertTrue(base64ArchiveDecompressButtonElements.size() > 0);
        try {
            base64ArchiveDecompressButtonElements.get(0).click();
        } catch (Exception error) {
            error.printStackTrace();
        }
        Thread.sleep(200);
        // Since we don't want to see unnesseray elements on the screen, the test value is added to the accessibilityId
        // If the accessibilityId of the button has been updated and includes the target value, the test is successful
        List<MobileElement> updatedBase64ArchiveDecompressButtonElements = driver.findElementsByAccessibilityId("DemoApp_Base64ArchiveTestComponent_Decompress_Button CgtQaW5nU2VydmljZRIEUGluZxoGCgR0ZXN0IAU=");
        System.out.println("Updated base64 decompress button elements count:");
        System.out.println(updatedBase64ArchiveDecompressButtonElements.size());
        Assert.assertTrue(updatedBase64ArchiveDecompressButtonElements.size() > 0);
        System.out.println("Base64Archive decompress method returned the target value");
    }

   @After
   public void after() throws Exception {
        if (driver != null) {
            ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            System.out.println("Stopping App");
            driver.quit();
        }
   }
}
