// Do not change this to index.ts or index.tsx, because
// App Center only requires index.js or index.ios.js/index.android.js for starting build

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
