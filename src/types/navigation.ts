import {NavigatorScreenParams} from '@react-navigation/native';
import {NightOwlDemoAppTabParamList} from 'NightOwlDemoApp';

// Routed for the main app
export type RootStackParamList = {
	RootScreen: undefined;
	NightOwlDemoApp: NavigatorScreenParams<NightOwlDemoAppTabParamList>;
	NightOwlApp: undefined;
};
