import {IPType} from '@goe/night-owl-transport';

/**
 * App global configuration
 */
export interface IConfig {
	/**
	 * Set to `true` for development version, `false` for release version
	 */
	isDevelop: boolean;

	/**
	 * Indicates IP address type (IPv4 | IPv6) to be set as preferred
	 * IP addresses of this type will be used first while establishing Local WebRTC connection
	 * with chargers. If the preferred type address is not available, the alternative type to be used
	 */
	preferredIPType: Exclude<IPType, 'NotValid'>;
}
