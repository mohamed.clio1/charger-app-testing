import {PayloadAction} from '@reduxjs/toolkit';
import {
	addLocalWebRTCDeviceAction,
	addNewBluetoothDeviceAction,
	moveBackFromSelectedChargerAction,
	moveToSelectedChargerAction,
	nightOwlDemoAppSlice,
	removeBluetoothDeviceAction,
	removeLocalWebRTCDeviceAction,
	setIPAdderessAvailabilityAction,
} from '../src/redux/slices/nightOwlDemoAppSlice';
import {INightOwlDemoAppState} from '../src/types/reduxStore';
import {chargerDeviceDefault, IChargerDevice, IIPAddress} from '@goe/night-owl-transport';
import * as actionTypes from '../src/types/actions';

const ipAddressV4: string = '192.168.0.1';
const ipAddressV6: string = '1:2:3:4:5:6:7:8';
const ipAddressNotValid: string = 'Not an IP';
const getIPAddressV4 = (): IIPAddress => ({
	type: 'IPv4',
	ipAddress: ipAddressV4,
	isAvailable: null,
});
const getIPAddressV6 = (): IIPAddress => ({
	type: 'IPv6',
	ipAddress: ipAddressV6,
	isAvailable: null,
});
const getIPAddressNotValid = (): IIPAddress => ({
	type: 'NotValid',
	ipAddress: ipAddressNotValid,
	isAvailable: false,
});
const manufacturerData = 'AAABAAAABgAAADM0RFg1SDJESkc1RVU4VErHqgE=';
const serialNumber = '34DX5H2DJG5EU8TJ';

test('Add a Bluetooth device as a new device', () => {
	const initialState: INightOwlDemoAppState = {
		chargerDevices: [],
	};

	const newCharger: IChargerDevice = {
		...chargerDeviceDefault,
		availableTransports: [0],
		serialNumber: serialNumber,
	};
	const expectedState: INightOwlDemoAppState = {
		chargerDevices: [newCharger],
	};

	const action: PayloadAction<actionTypes.IAddNewBluetoothDeviceActionPayload> = addNewBluetoothDeviceAction({
		manufacturerData,
	});
	const actualState: INightOwlDemoAppState = nightOwlDemoAppSlice.reducer(initialState, action);

	// JSON.stringify used as workaround of a known Jest issue
	expect(JSON.stringify(actualState)).toBe(JSON.stringify(expectedState));
});

test('Add a Bluetooth device; a WebRTC device with the same serial number already exists', () => {
	const existingCharger: IChargerDevice = {
		...chargerDeviceDefault,
		availableTransports: [1],
		ipAddressList: [getIPAddressV4(), getIPAddressV6()],
		serialNumber: serialNumber,
	};
	const initialState: INightOwlDemoAppState = {
		chargerDevices: [existingCharger],
	};

	const updatedCharger: IChargerDevice = {
		...chargerDeviceDefault,
		availableTransports: [1, 0],
		ipAddressList: [getIPAddressV4(), getIPAddressV6()],
		serialNumber: serialNumber,
	};
	const expectedState: INightOwlDemoAppState = {
		chargerDevices: [updatedCharger],
	};

	const action: PayloadAction<actionTypes.IAddNewBluetoothDeviceActionPayload> = addNewBluetoothDeviceAction({
		manufacturerData,
	});
	const actualState: INightOwlDemoAppState = nightOwlDemoAppSlice.reducer(initialState, action);

	// JSON.stringify used as workaround of a known Jest issue
	expect(JSON.stringify(actualState)).toBe(JSON.stringify(expectedState));
});

test('Remove a Bluetooth device; a WebRTC device with the same serial number also exists', () => {
	const existingCharger: IChargerDevice = {
		...chargerDeviceDefault,
		availableTransports: [0, 1],
		ipAddressList: [getIPAddressV4(), getIPAddressV6()],
		serialNumber: serialNumber,
	};
	const initialState: INightOwlDemoAppState = {
		chargerDevices: [existingCharger],
	};

	const updatedCharger: IChargerDevice = {
		...chargerDeviceDefault,
		availableTransports: [1],
		ipAddressList: [getIPAddressV4(), getIPAddressV6()],
		serialNumber: serialNumber,
	};
	const expectedState: INightOwlDemoAppState = {
		chargerDevices: [updatedCharger],
	};

	const action: PayloadAction<actionTypes.IRemoveBluetoothDeviceActionPayload> = removeBluetoothDeviceAction({
		serialNumber,
	});
	const actualState: INightOwlDemoAppState = nightOwlDemoAppSlice.reducer(initialState, action);

	// JSON.stringify used as workaround of a known Jest issue
	expect(JSON.stringify(actualState)).toBe(JSON.stringify(expectedState));
});

test('Remove a Bluetooth device; no WebRTC device with the same serial number exists', () => {
	const existingCharger: IChargerDevice = {
		...chargerDeviceDefault,
		availableTransports: [0],
		serialNumber: serialNumber,
	};
	const initialState: INightOwlDemoAppState = {
		chargerDevices: [existingCharger],
	};

	const expectedState: INightOwlDemoAppState = {
		chargerDevices: [],
	};

	const action: PayloadAction<actionTypes.IRemoveBluetoothDeviceActionPayload> = removeBluetoothDeviceAction({
		serialNumber,
	});
	const actualState: INightOwlDemoAppState = nightOwlDemoAppSlice.reducer(initialState, action);

	// JSON.stringify used as workaround of a known Jest issue
	expect(JSON.stringify(actualState)).toBe(JSON.stringify(expectedState));
});

test('Add a WebRTC device as a new device', () => {
	const initialState: INightOwlDemoAppState = {
		chargerDevices: [],
	};

	const newCharger: IChargerDevice = {
		...chargerDeviceDefault,
		availableTransports: [1],
		ipAddressList: [getIPAddressV4(), getIPAddressV6()],
		serialNumber: serialNumber,
	};
	const expectedState: INightOwlDemoAppState = {
		chargerDevices: [newCharger],
	};

	const action: PayloadAction<actionTypes.IAddLocalWebRTCServiceActionPayload> = addLocalWebRTCDeviceAction({
		serialNumber: serialNumber,
		ipAddressStringList: [ipAddressV4, ipAddressV6],
	});
	const actualState: INightOwlDemoAppState = nightOwlDemoAppSlice.reducer(initialState, action);

	// JSON.stringify used as workaround of a known Jest issue
	expect(JSON.stringify(actualState)).toBe(JSON.stringify(expectedState));
});

test('Add a WebRTC device; a Bluetooth device with the same serial number also exists', () => {
	const existingCharger: IChargerDevice = {
		...chargerDeviceDefault,
		availableTransports: [0],
		serialNumber: serialNumber,
	};
	const initialState: INightOwlDemoAppState = {
		chargerDevices: [existingCharger],
	};

	const updatedCharger: IChargerDevice = {
		...chargerDeviceDefault,
		availableTransports: [0, 1],
		ipAddressList: [getIPAddressV4(), getIPAddressV6()],
		serialNumber: serialNumber,
	};
	const expectedState: INightOwlDemoAppState = {
		chargerDevices: [updatedCharger],
	};

	const action: PayloadAction<actionTypes.IAddLocalWebRTCServiceActionPayload> = addLocalWebRTCDeviceAction({
		serialNumber: serialNumber,
		ipAddressStringList: [ipAddressV4, ipAddressV6],
	});
	const actualState: INightOwlDemoAppState = nightOwlDemoAppSlice.reducer(initialState, action);

	// JSON.stringify used as workaround of a known Jest issue
	expect(JSON.stringify(actualState)).toBe(JSON.stringify(expectedState));
});

test('Update a WebRTC device: add new IP address data', () => {
	const existingCharger: IChargerDevice = {
		...chargerDeviceDefault,
		availableTransports: [1],
		ipAddressList: [getIPAddressV4()],
		serialNumber: serialNumber,
	};
	const initialState: INightOwlDemoAppState = {
		chargerDevices: [existingCharger],
	};

	const updatedCharger: IChargerDevice = {
		...chargerDeviceDefault,
		availableTransports: [1],
		ipAddressList: [getIPAddressV4(), getIPAddressV6(), getIPAddressNotValid()],
		serialNumber: serialNumber,
	};
	const expectedState: INightOwlDemoAppState = {
		chargerDevices: [updatedCharger],
	};

	const action: PayloadAction<actionTypes.IAddLocalWebRTCServiceActionPayload> = addLocalWebRTCDeviceAction({
		serialNumber: serialNumber,
		ipAddressStringList: [ipAddressV4, ipAddressV6, ipAddressNotValid],
	});
	const actualState: INightOwlDemoAppState = nightOwlDemoAppSlice.reducer(initialState, action);

	// JSON.stringify used as workaround of a known Jest issue
	expect(JSON.stringify(actualState)).toBe(JSON.stringify(expectedState));
});

test('Remove a local WebRTC device; a Bluetooth device with the same serial number also exists', () => {
	const existingCharger: IChargerDevice = {
		...chargerDeviceDefault,
		availableTransports: [1, 0],
		ipAddressList: [getIPAddressV4(), getIPAddressV6()],
		serialNumber: serialNumber,
	};
	const initialState: INightOwlDemoAppState = {
		chargerDevices: [existingCharger],
	};

	const updatedCharger: IChargerDevice = {
		...chargerDeviceDefault,
		availableTransports: [0],
		serialNumber: serialNumber,
	};
	const expectedState: INightOwlDemoAppState = {
		chargerDevices: [updatedCharger],
	};

	const action: PayloadAction<actionTypes.IRemoveLocalWebRTCServiceActionPayload> = removeLocalWebRTCDeviceAction({
		serialNumber,
	});
	const actualState: INightOwlDemoAppState = nightOwlDemoAppSlice.reducer(initialState, action);

	// JSON.stringify used as workaround of a known Jest issue
	expect(JSON.stringify(actualState)).toBe(JSON.stringify(expectedState));
});

test('Remove a local WebRTC device; no Bluetooth device with the same serial number exists', () => {
	const existingCharger: IChargerDevice = {
		...chargerDeviceDefault,
		availableTransports: [1],
		ipAddressList: [getIPAddressV4(), getIPAddressV6()],
		serialNumber: serialNumber,
	};
	const initialState: INightOwlDemoAppState = {
		chargerDevices: [existingCharger],
	};

	const expectedState: INightOwlDemoAppState = {
		chargerDevices: [],
	};

	const action: PayloadAction<actionTypes.IRemoveLocalWebRTCServiceActionPayload> = removeLocalWebRTCDeviceAction({
		serialNumber,
	});
	const actualState: INightOwlDemoAppState = nightOwlDemoAppSlice.reducer(initialState, action);

	// JSON.stringify used as workaround of a known Jest issue
	expect(JSON.stringify(actualState)).toBe(JSON.stringify(expectedState));
});

test('Navigate to chosen charger screen', () => {
	const existingCharger: IChargerDevice = {
		...chargerDeviceDefault,
		availableTransports: [0],
		serialNumber: serialNumber,
	};
	const initialState: INightOwlDemoAppState = {
		chargerDevices: [existingCharger],
	};

	const updatedCharger: IChargerDevice = {
		...chargerDeviceDefault,
		availableTransports: [0],
		serialNumber: serialNumber,
		isSelected: true,
	};
	const expectedState: INightOwlDemoAppState = {
		chargerDevices: [updatedCharger],
	};

	const action: PayloadAction<actionTypes.IMoveToSelectedChargerActionPayload> = moveToSelectedChargerAction({
		serialNumber,
	});
	const actualState: INightOwlDemoAppState = nightOwlDemoAppSlice.reducer(initialState, action);

	// JSON.stringify used as workaround of a known Jest issue
	expect(JSON.stringify(actualState)).toBe(JSON.stringify(expectedState));
});

test('Navigate back from charger screen', () => {
	const existingCharger: IChargerDevice = {
		...chargerDeviceDefault,
		availableTransports: [0],
		serialNumber: serialNumber,
		isSelected: true,
	};
	const initialState: INightOwlDemoAppState = {
		chargerDevices: [existingCharger],
	};

	const updatedCharger: IChargerDevice = {
		...chargerDeviceDefault,
		availableTransports: [0],
		serialNumber: serialNumber,
	};
	const expectedState: INightOwlDemoAppState = {
		chargerDevices: [updatedCharger],
	};

	const action: PayloadAction<actionTypes.IMoveBackFromSelectedChargerActionPayload> =
		moveBackFromSelectedChargerAction();
	const actualState: INightOwlDemoAppState = nightOwlDemoAppSlice.reducer(initialState, action);

	// JSON.stringify used as workaround of a known Jest issue
	expect(JSON.stringify(actualState)).toBe(JSON.stringify(expectedState));
});

test('Marking IP address of a charger as unavailable', () => {
	const existingCharger: IChargerDevice = {
		...chargerDeviceDefault,
		availableTransports: [1],
		ipAddressList: [getIPAddressV4(), getIPAddressV6()],
		serialNumber: serialNumber,
	};
	const initialState: INightOwlDemoAppState = {
		chargerDevices: [existingCharger],
	};

	const updatedCharger: IChargerDevice = {
		...chargerDeviceDefault,
		availableTransports: [1],
		ipAddressList: [{...getIPAddressV4(), isAvailable: false}, getIPAddressV6()],
		serialNumber: serialNumber,
	};
	const expectedState: INightOwlDemoAppState = {
		chargerDevices: [updatedCharger],
	};

	const action: PayloadAction<actionTypes.ISetIPAdderessAvailabilityActionPayload> = setIPAdderessAvailabilityAction({
		selectedIpAddressString: getIPAddressV4().ipAddress,
		isAvailable: false,
		chargerSerialNumber: serialNumber,
	});
	const actualState: INightOwlDemoAppState = nightOwlDemoAppSlice.reducer(initialState, action);

	// JSON.stringify used as workaround of a known Jest issue
	expect(JSON.stringify(actualState)).toBe(JSON.stringify(expectedState));
});
