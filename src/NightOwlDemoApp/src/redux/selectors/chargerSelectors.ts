import {IChargerDevice} from '@goe/night-owl-transport';
import {IRootState} from '../../../../../App';

/**
 * Gets charger selected by user, if any
 * @param state - Redux store
 * @returns Selected charger or null
 */
export const selectedChargerSelector = (state: IRootState): IChargerDevice | null =>
	state.nightOwlDemoApp.chargerDevices.find(chargerDevice => chargerDevice.isSelected) || null;
