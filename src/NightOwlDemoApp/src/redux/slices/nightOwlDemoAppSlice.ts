import {createSlice, PayloadAction} from '@reduxjs/toolkit';

// utils
import {
	addBluetoothDevice,
	addLocalWebRTCDevice,
	deselectCharger,
	removeBluetoothDevice,
	removeLocalWebRTCDevice,
	selectCharger,
	setIPAddressAvailability,
	setLocalWebRTCConnectionStatus,
} from '@goe/night-owl-transport';

// types
import {INightOwlDemoAppState} from '../../types/reduxStore';
import {IChargerDevice} from '@goe/night-owl-transport';
import * as actionTypes from '../../types/actions';

const initialState: INightOwlDemoAppState = {
	chargerDevices: [],
};

export const nightOwlDemoAppSlice = createSlice({
	name: 'nightOwlDemoApp',
	initialState,
	reducers: {
		addNewBluetoothDeviceAction: (
			state: INightOwlDemoAppState,
			action: PayloadAction<actionTypes.IAddNewBluetoothDeviceActionPayload>,
		): void => {
			const {manufacturerData} = action.payload;
			const updatedChargerDevices: IChargerDevice[] = addBluetoothDevice(state.chargerDevices, manufacturerData);
			state.chargerDevices = updatedChargerDevices;
		},
		removeBluetoothDeviceAction: (
			state: INightOwlDemoAppState,
			action: PayloadAction<actionTypes.IRemoveBluetoothDeviceActionPayload>,
		): void => {
			const {serialNumber} = action.payload;
			const updatedChargerDevices: IChargerDevice[] = removeBluetoothDevice(state.chargerDevices, serialNumber);
			state.chargerDevices = updatedChargerDevices;
		},
		addLocalWebRTCDeviceAction: (
			state: INightOwlDemoAppState,
			action: PayloadAction<actionTypes.IAddLocalWebRTCServiceActionPayload>,
		): void => {
			const {serialNumber, ipAddressStringList} = action.payload;
			const updatedChargerDevices: IChargerDevice[] = addLocalWebRTCDevice(
				state.chargerDevices,
				serialNumber,
				ipAddressStringList,
			);
			state.chargerDevices = updatedChargerDevices;
		},
		removeLocalWebRTCDeviceAction: (
			state: INightOwlDemoAppState,
			action: PayloadAction<actionTypes.IRemoveLocalWebRTCServiceActionPayload>,
		): void => {
			const {serialNumber} = action.payload;
			const updatedChargerDevices: IChargerDevice[] = removeLocalWebRTCDevice(state.chargerDevices, serialNumber);
			state.chargerDevices = updatedChargerDevices;
		},
		setLocalWebRTCConnectionStatusAction: (
			state: INightOwlDemoAppState,
			action: PayloadAction<actionTypes.ISetLocalWebRTCConnectionStatusActionPayload>,
		): void => {
			const {serialNumber, isConnected} = action.payload;
			const updatedChargerDevices: IChargerDevice[] = setLocalWebRTCConnectionStatus(
				state.chargerDevices,
				serialNumber,
				isConnected,
			);
			state.chargerDevices = updatedChargerDevices;
		},
		setIPAdderessAvailabilityAction: (
			state: INightOwlDemoAppState,
			action: PayloadAction<actionTypes.ISetIPAdderessAvailabilityActionPayload>,
		): void => {
			const {selectedIpAddressString, isAvailable, chargerSerialNumber} = action.payload;
			const updatedChargerDevices: IChargerDevice[] = setIPAddressAvailability(
				state.chargerDevices,
				selectedIpAddressString,
				isAvailable,
				chargerSerialNumber,
			);
			state.chargerDevices = updatedChargerDevices;
		},
		moveToSelectedChargerAction: (
			state: INightOwlDemoAppState,
			action: PayloadAction<actionTypes.IMoveToSelectedChargerActionPayload>,
		): void => {
			const {serialNumber} = action.payload;
			const updatedChargerDevices: IChargerDevice[] = selectCharger(state.chargerDevices, serialNumber);
			state.chargerDevices = updatedChargerDevices;
		},
		moveBackFromSelectedChargerAction: (state: INightOwlDemoAppState): void => {
			const updatedChargerDevices: IChargerDevice[] = deselectCharger(state.chargerDevices);
			state.chargerDevices = updatedChargerDevices;
		},
	},
});

export const {
	addNewBluetoothDeviceAction,
	removeBluetoothDeviceAction,
	addLocalWebRTCDeviceAction,
	removeLocalWebRTCDeviceAction,
	setLocalWebRTCConnectionStatusAction,
	setIPAdderessAvailabilityAction,
	moveToSelectedChargerAction,
	moveBackFromSelectedChargerAction,
} = nightOwlDemoAppSlice.actions;
