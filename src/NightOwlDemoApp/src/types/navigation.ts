import {NavigatorScreenParams} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import {IPType} from '@goe/night-owl-transport';

// Routes for NightOwl Demo App
export type NightOwlDemoAppTabParamList = {
	TransportDemo: {preferredIPType: IPType} & NavigatorScreenParams<TransportDemoStackParamList>;
	ComponentsDemo: undefined;
};

// Routes for Transport Demo section
export type TransportDemoStackParamList = {
	HomeScreen: undefined;
	ChargerScreen: undefined;
	SettingsScreen: undefined;
};
// Navigation prop for Home screen of Transport Demo section
export type TransportDemoHomeScreenNavigationProps = StackNavigationProp<TransportDemoStackParamList, 'HomeScreen'>;
// Navigation prop for Charger screen of Transport Demo section
export type TransportDemoChargerScreenNavigationProps = StackNavigationProp<
	TransportDemoStackParamList,
	'ChargerScreen'
>;
