export interface IAddNewBluetoothDeviceActionPayload {
	manufacturerData: string | null;
}

export interface IRemoveBluetoothDeviceActionPayload {
	serialNumber: string | null;
}

export interface IAddLocalWebRTCServiceActionPayload {
	serialNumber: string | null;
	ipAddressStringList: string[];
}

export interface IRemoveLocalWebRTCServiceActionPayload {
	serialNumber: string | null;
}

export interface ISetLocalWebRTCConnectionStatusActionPayload {
	serialNumber: string;
	isConnected: boolean;
}

export interface ISetIPAdderessAvailabilityActionPayload {
	selectedIpAddressString: string;
	isAvailable: boolean;
	chargerSerialNumber: string;
}

export interface IMoveToSelectedChargerActionPayload {
	serialNumber: string;
}

export type IMoveBackFromSelectedChargerActionPayload = undefined;
