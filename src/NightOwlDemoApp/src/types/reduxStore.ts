import {IChargerDevice} from '@goe/night-owl-transport';

export interface INightOwlDemoAppState {
	chargerDevices: IChargerDevice[];
}
