import React from 'react';

// Redux
import {connect} from 'react-redux';
import {selectedChargerSelector} from '../redux/selectors/chargerSelectors';
import {addNewBluetoothDeviceAction, removeBluetoothDeviceAction} from '../redux/slices/nightOwlDemoAppSlice';
import {IDispatch, IRootState} from '../../../../App';

// Context provider
import {BluetoothContextProvider} from '@goe/night-owl-transport';

// Types
import {IChargerDevice} from '@goe/night-owl-transport';

/**
 * Component's direct props (controlled by developer)
 */
interface IOwnProps {
	children: React.ReactNode;
}

/**
 * Props received from Redux Store
 */
interface IStateProps {
	/**
	 * A list of discovered chargers
	 */
	chargerDevices: IChargerDevice[];

	/**
	 * Charger chosen by user
	 */
	selectedChargerDevice: IChargerDevice | null;
}

/**
 * Props for sending actions to Redux Store
 */
interface IDispatchProps {
	/**
	 * Adds charger's data discovered by Bluetooth scan to Redux Store
	 * @param manufacturerData - Manufacturer data of charger's Bluetooth module.
	 * Contains encoded serial number of newly discovered charger
	 */
	addNewBluetoothDevice: (manufacturerData: string | null) => void;

	/**
	 * Remove charger's data from Redux Store if it was not discovered by Bluetooth scan
	 * @param serialNumber - Serial number of the charger which stopped advertising
	 */
	removeExistingBluetoothDevice: (serialNumber: string | null) => void;
}

type IProps = IOwnProps & IStateProps & IDispatchProps;

const BluetoothContextProviderContainer: React.FC<IProps> = ({
	children,
	chargerDevices,
	selectedChargerDevice,
	addNewBluetoothDevice,
	removeExistingBluetoothDevice,
}: IProps): React.ReactElement<IProps> => {
	return (
		<BluetoothContextProvider
			chargerDevices={chargerDevices}
			selectedChargerDevice={selectedChargerDevice}
			addNewBluetoothDevice={addNewBluetoothDevice}
			removeExistingBluetoothDevice={removeExistingBluetoothDevice}
		>
			{children}
		</BluetoothContextProvider>
	);
};

const mapStateToProps = (state: IRootState): IStateProps => ({
	chargerDevices: state.nightOwlDemoApp.chargerDevices,
	selectedChargerDevice: selectedChargerSelector(state),
});

const mapDispatchToProps = (dispatch: IDispatch): IDispatchProps => ({
	addNewBluetoothDevice: (manufacturerData: string | null) => dispatch(addNewBluetoothDeviceAction({manufacturerData})),
	removeExistingBluetoothDevice: (serialNumber: string | null) => dispatch(removeBluetoothDeviceAction({serialNumber})),
});

export default connect(mapStateToProps, mapDispatchToProps)(BluetoothContextProviderContainer);
