import React from 'react';

// Redux
import {connect} from 'react-redux';
import {selectedChargerSelector} from '../redux/selectors/chargerSelectors';
import {
	addLocalWebRTCDeviceAction,
	removeLocalWebRTCDeviceAction,
	setLocalWebRTCConnectionStatusAction,
	setIPAdderessAvailabilityAction,
} from '../redux/slices/nightOwlDemoAppSlice';
import {IDispatch, IRootState} from '../../../../App';

// Context provider
import {LocalWebRTCContextProvider} from '@goe/night-owl-transport';

// Types
import {IChargerDevice, IPType} from '@goe/night-owl-transport';

/**
 * Component's direct props (controlled by developer)
 */
interface IOwnProps {
	/**
	 * The type of IP address (IPv4/IPv6) to be tested first
	 */
	preferredIPType: IPType;
	children: React.ReactNode;
}

/**
 * Props received from Redux Store
 */
interface IStateProps {
	/**
	 * A list of discovered chargers
	 */
	chargerDevices: IChargerDevice[];

	/**
	 * Charger chosen by user
	 */
	selectedChargerDevice: IChargerDevice | null;
}

/**
 * Props for sending actions to Redux Store
 */
interface IDispatchProps {
	/**
	 * Adds charger's data discovered by Zeroconf scan to Redux Store
	 * @param serialNumber - Serial number of newly discovered charger
	 * @param ipAddressStringList - Array of IP addresses of newly discovered charger
	 */
	addLocalWebRTCService: (serialNumber: string, ipAddressStringList: string[]) => void;

	/**
	 * Remove charger's data from Redux Store if it was not discovered by Zeroconf scan
	 * @param serialNumber - Serial number of the charger which stopped advertising
	 */
	removeExistingLocalWebRTCDevice: (serialNumber: string) => void;

	/**
	 * Changes WebRTC connection status (online/offline) for a given charger
	 * @param serialNumber - Serial number of the charger whose status changed
	 * @param isChargerConnected - Boolean value reflecting whether WebRTC connection with
	 * the charger has been established
	 */
	setChargerWebRTCConnectionStatus: (chargerSerialNumber: string, isChargerConnected: boolean) => void;

	/**
	 * Marks IP address for the current charger as available or unavailable
	 * @param selectedIpAddressString - IP address whose status changed
	 * @isAvailable - Boolean value reflecting whether the IP address is available or unavailable
	 * @param chargerSerialNumber - Serial number of the charger whose IP address status changed
	 */
	setIPAdderessAvailability: (
		selectedIpAddressString: string,
		isAvailable: boolean,
		chargerSerialNumber: string,
	) => void;
}

type IProps = IOwnProps & IStateProps & IDispatchProps;

const LocalWebRTCContextProviderContainer: React.FC<IProps> = ({
	preferredIPType,
	children,
	chargerDevices,
	selectedChargerDevice,
	addLocalWebRTCService,
	removeExistingLocalWebRTCDevice,
	setChargerWebRTCConnectionStatus,
	setIPAdderessAvailability,
}: IProps): React.ReactElement<IProps> => {
	return (
		<LocalWebRTCContextProvider
			preferredIPType={preferredIPType}
			chargerDevices={chargerDevices}
			selectedChargerDevice={selectedChargerDevice}
			addLocalWebRTCService={addLocalWebRTCService}
			removeExistingLocalWebRTCDevice={removeExistingLocalWebRTCDevice}
			setChargerWebRTCConnectionStatus={setChargerWebRTCConnectionStatus}
			setIPAdderessAvailability={setIPAdderessAvailability}
		>
			{children}
		</LocalWebRTCContextProvider>
	);
};

const mapStateToProps = (state: IRootState): IStateProps => ({
	chargerDevices: state.nightOwlDemoApp.chargerDevices,
	selectedChargerDevice: selectedChargerSelector(state),
});

const mapDispatchToProps = (dispatch: IDispatch): IDispatchProps => ({
	addLocalWebRTCService: (serialNumber: string, ipAddressStringList: string[]) =>
		dispatch(addLocalWebRTCDeviceAction({serialNumber, ipAddressStringList})),
	removeExistingLocalWebRTCDevice: (serialNumber: string) => dispatch(removeLocalWebRTCDeviceAction({serialNumber})),
	setChargerWebRTCConnectionStatus: (chargerSerialNumber: string, isChargerConnected: boolean) =>
		dispatch(
			setLocalWebRTCConnectionStatusAction({serialNumber: chargerSerialNumber, isConnected: isChargerConnected}),
		),
	setIPAdderessAvailability: (selectedIpAddressString: string, isAvailable: boolean, chargerSerialNumber: string) =>
		dispatch(setIPAdderessAvailabilityAction({selectedIpAddressString, isAvailable, chargerSerialNumber})),
});

export default connect(mapStateToProps, mapDispatchToProps)(LocalWebRTCContextProviderContainer);
