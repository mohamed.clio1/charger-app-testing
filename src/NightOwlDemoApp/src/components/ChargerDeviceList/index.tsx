import React from 'react';
import {TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import {v4 as uuidv4} from 'uuid';

import {IChargerDevice} from '@goe/night-owl-transport';
import {IDispatch, IRootState} from '../../../../../App';

import {moveToSelectedChargerAction} from '../../redux/slices/nightOwlDemoAppSlice';
import {ChargerDeviceItem} from '../ChargerDeviceItem';
import {TransportDemoHomeScreenNavigationProps} from '../../types/navigation';

interface IStateProps {
	chargerDevices: IChargerDevice[];
}

interface IDispatchProps {
	moveToSelectedCharger: (serialNumber: string) => void;
}

type Props = {
	navigation: TransportDemoHomeScreenNavigationProps;
};

const ChargerDeviceList = (props: IStateProps & IDispatchProps & Props) => {
	const onChargerPress = (serialNumber: string) => {
		props.navigation.navigate('ChargerScreen');
		props.moveToSelectedCharger(serialNumber);
	};

	return (
		<View>
			{props.chargerDevices?.map(chargerDevice => {
				return (
					<TouchableOpacity
						accessible={false}
						accessibilityLabel={`DemoApp_TransportSegment_ChargerListItem_${chargerDevice.serialNumber}`}
						key={uuidv4()}
						onPress={() => onChargerPress(chargerDevice.serialNumber)}
					>
						<ChargerDeviceItem chargerDevice={chargerDevice} />
					</TouchableOpacity>
				);
			}) || null}
		</View>
	);
};

const mapStateToProps = (state: IRootState) => ({
	chargerDevices: state.nightOwlDemoApp.chargerDevices,
});

const mapDispatchToProps = (dispatch: IDispatch) => ({
	moveToSelectedCharger: (serialNumber: string) => dispatch(moveToSelectedChargerAction({serialNumber})),
});

const ChargerDeviceListContainer = connect(mapStateToProps, mapDispatchToProps)(ChargerDeviceList);

export default ChargerDeviceListContainer;
