import React, {useContext} from 'react';
import {View} from 'react-native';
import {LocalWebRTCContext} from '@goe/night-owl-transport';
import {commomStyles} from '../styles/commonStyles';

const LocalWebRTCSearchIndicator = () => {
	const {isLocalWebRTCSearchEnabled} = useContext(LocalWebRTCContext);
	return isLocalWebRTCSearchEnabled ? (
		<View style={[commomStyles.searchingIndicatorSize, commomStyles.searchingIndicatorOrange]} />
	) : (
		<View style={[commomStyles.searchingIndicatorSize, commomStyles.searchingIndicatorRed]} />
	);
};

export default LocalWebRTCSearchIndicator;
