import {StyleSheet} from 'react-native';

export const commomStyles = StyleSheet.create({
	indicatorImage: {
		width: 20,
		height: 20,
	},
	searchingIndicatorSize: {
		width: '100%',
		height: 3,
	},
	searchingIndicatorBlue: {
		backgroundColor: 'blue',
	},
	searchingIndicatorRed: {
		backgroundColor: 'red',
	},
	searchingIndicatorOrange: {
		backgroundColor: 'orange',
	},
});
