import React, {useContext} from 'react';
import {View} from 'react-native';
import {BluetoothContext} from '@goe/night-owl-transport';
import {commomStyles} from '../styles/commonStyles';

const BluetoothSearchIndicator = () => {
	const {isBluetoothSearchEnabled} = useContext(BluetoothContext);
	return isBluetoothSearchEnabled ? (
		<View style={[commomStyles.searchingIndicatorSize, commomStyles.searchingIndicatorBlue]} />
	) : (
		<View style={[commomStyles.searchingIndicatorSize, commomStyles.searchingIndicatorRed]} />
	);
};

export default BluetoothSearchIndicator;
