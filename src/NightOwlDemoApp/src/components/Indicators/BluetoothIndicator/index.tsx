import {Image} from 'react-native';
import {commomStyles} from '../styles/commonStyles';

import React from 'react';

import BTIcon from '../../../assets/icons/bt.png';

export const BluetoothIndicator = () => <Image source={BTIcon} style={commomStyles.indicatorImage} />;
