import React from 'react';
import {Image, StyleProp, View, ViewStyle} from 'react-native';
import WebRTCIcon from '../../../assets/icons/webrtc.png';
import {commomStyles} from '../styles/commonStyles';

interface IProps {
	isConnected: boolean;
}

export const LocalWebRTCIndicator: React.FC<IProps> = ({isConnected}: IProps): React.ReactElement<View> => {
	const backgroundStyle: StyleProp<ViewStyle> = {backgroundColor: isConnected ? 'green' : 'transparent'};

	return (
		<View style={backgroundStyle}>
			<Image source={WebRTCIcon} style={commomStyles.indicatorImage} />
		</View>
	);
};
