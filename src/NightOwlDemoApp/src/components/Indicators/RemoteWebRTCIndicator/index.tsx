// FIXME Link to Remote WebRTC context when ready
import {StyleSheet, Text, View} from 'react-native';
import React from 'react';

export const InternetIndicator = () => (
	<View style={styles.container}>
		<Text style={styles.text}>Internet</Text>
	</View>
);

const styles = StyleSheet.create({
	container: {
		flexDirection: 'column',
		width: 48,
		alignItems: 'center',
	},
	text: {
		fontSize: 12,
	},
});
