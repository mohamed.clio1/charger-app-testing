import React from 'react';
import {ActivityIndicator, Modal, Platform, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {
	BluetoothErrorType,
	BluetoothServiceName,
	LocalWebRTCErrorType,
	LocalWebRTCServiceName,
} from '@goe/night-owl-transport';

export interface ITimedMessage {
	message: string;
	timeStamp: number;
}

export interface IServiceAnswerConsoleMessage {
	serviceType: LocalWebRTCServiceName | BluetoothServiceName | BluetoothErrorType | LocalWebRTCErrorType;
	messageList: ITimedMessage[];
}

export interface IMessageConsoleModalProps {
	isVisible: boolean;
	onClose: () => void;
	messageList: null | IServiceAnswerConsoleMessage[];
}

// Component name for accessibility label
const componentName: string = 'DemoApp_TransportSection_MessageConsoleModal';

const MessageConsoleModal: React.FC<IMessageConsoleModalProps> = ({
	isVisible,
	messageList,
	onClose,
}: IMessageConsoleModalProps): React.ReactElement<IMessageConsoleModalProps> => (
	<Modal animationType="slide" transparent={true} visible={isVisible}>
		<View accessibilityLabel={isVisible ? componentName : undefined} style={styles.container}>
			<TouchableOpacity
				accessibilityLabel={isVisible ? `${componentName}_Close_Button` : undefined}
				style={styles.closeButton}
				onPress={onClose}
			>
				<Text
					accessibilityLabel={isVisible ? `${componentName}_Close_Button_Text` : undefined}
					style={styles.closeButtonText}
				>
					Close
				</Text>
			</TouchableOpacity>
			{messageList ? (isVisible ? renderMessageBlockList(messageList) : null) : renderActivityIndicator()}
		</View>
	</Modal>
);

const renderMessageBlockList = (messageBlockList: IServiceAnswerConsoleMessage[]): React.ReactElement<ScrollView> => (
	<ScrollView style={styles.messageListContainer}>
		{messageBlockList.map(
			(messageBlock: IServiceAnswerConsoleMessage): React.ReactElement<Text> => (
				<View
					accessibilityLabel={`${componentName}_MessageBlock_${getMessageTypeName(messageBlock.serviceType)}`}
					key={`${JSON.stringify(messageBlock)}`}
				>
					<Text
						accessibilityLabel={`${componentName}_MessageBlock_${getMessageTypeName(messageBlock.serviceType)}_Text`}
						style={styles.messageTypeNameText}
					>
						{getMessageTypeName(messageBlock.serviceType)}
					</Text>
					{renderMessageList(messageBlock.messageList)}
				</View>
			),
		)}
	</ScrollView>
);

const renderActivityIndicator = (): React.ReactElement<ActivityIndicator> => (
	<View style={styles.activityIndicatorContainer}>
		<ActivityIndicator accessibilityLabel={`${componentName}_ActivityIndicator`} color={'white'} size="large" />
	</View>
);

const renderMessageList = (messageList: ITimedMessage[]): React.ReactElement<Text>[] => {
	return messageList.map(
		(message: ITimedMessage, index: number): React.ReactElement<Text> => (
			<Text
				accessibilityLabel={`${componentName}_Message_${index}_Text`}
				key={JSON.stringify(message)}
				style={styles.messageText}
			>
				{message.message}
			</Text>
		),
	);
};

const getMessageTypeName = (
	serviceType: LocalWebRTCServiceName | BluetoothServiceName | BluetoothErrorType | LocalWebRTCErrorType,
): string => {
	switch (serviceType) {
		case 'PingService':
			return 'WebRTC Ping';
		case 'MetricsService':
			return 'WebRTC Metrics';
		case 'SetStateService':
			return 'WebRTC Setting State';
		case 'GetStateService':
			return 'WebRTC Retrieving State';

		case 'PingServiceBluetooth':
			return 'Bluetooth Ping';
		case 'MetricsServiceBluetooth':
			return 'Bluetooth Metrics';
		case 'SetStateServiceBluetooth':
			return 'Bluetooth Setting State';
		case 'GetStateServiceBluetooth':
			return 'Bluetooth Retrieving State';

		case 'LocalWebRTCError':
			return 'Local WebRTC Error';
		case 'BluetoothError':
			return 'Bluetooth Error';

		default:
			return 'Unknown service';
	}
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 10,
		paddingTop: Platform.OS === 'ios' ? 30 : 10,
		backgroundColor: 'rgba(0,0,0,0.8)',
	},
	closeButton: {
		padding: 8,
		backgroundColor: 'red',
		alignSelf: 'flex-end',
	},
	closeButtonText: {
		color: 'white',
		fontWeight: 'bold',
	},
	messageListContainer: {
		marginTop: 20,
	},
	messageTypeNameText: {
		fontSize: 18,
		fontWeight: '700',
		color: 'white',
	},
	messageText: {
		fontSize: 18,
		color: 'white',
	},
	activityIndicatorContainer: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
});

export default MessageConsoleModal;
