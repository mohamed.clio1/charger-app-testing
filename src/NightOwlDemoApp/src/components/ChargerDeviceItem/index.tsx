import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';

import ChargerLogo from '../../assets/charger/logo.png';

// Components
import {BluetoothIndicator} from '../Indicators/BluetoothIndicator';
import {LocalWebRTCIndicator} from '../Indicators/LocalWebRTCIndicator';
import {AvailableTransports} from '@goe/night-owl-transport';

// Utils
import {getIsLocalWebRTCAvailable} from '@goe/night-owl-transport';

// Types
import {IChargerDevice} from '@goe/night-owl-transport';

export const ChargerDeviceItem = (props: {chargerDevice: IChargerDevice}) => (
	<View style={styles.container}>
		<Image source={ChargerLogo} style={styles.image} />
		<View style={styles.serialNumberAndIndicatorsContainer}>
			<Text>{props.chargerDevice.serialNumber}</Text>
			<View style={styles.indicatorsContainer}>
				{props.chargerDevice.availableTransports.includes(AvailableTransports.Bluetooth) && <BluetoothIndicator />}
				{getIsLocalWebRTCAvailable(props.chargerDevice) && (
					<LocalWebRTCIndicator isConnected={props.chargerDevice.isWebRTCConnection} />
				)}
			</View>
		</View>
	</View>
);

const styles = StyleSheet.create({
	container: {
		width: '100%',
		backgroundColor: '#eeeeee',
		height: 80,
		marginBottom: 10,
		flexDirection: 'row',
		borderBottomWidth: 1,
		borderBottomColor: '#cccccc',
	},
	image: {
		width: 80,
		height: 79,
	},
	serialNumberAndIndicatorsContainer: {
		marginLeft: 10,
	},
	indicatorsContainer: {
		flexDirection: 'row',
	},
});
