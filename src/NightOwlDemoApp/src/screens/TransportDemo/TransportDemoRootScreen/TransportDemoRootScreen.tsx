import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

// Config
import {config} from '../../../../../config';

// Redux
import {connect} from 'react-redux';
import {moveBackFromSelectedChargerAction} from '../../../redux/slices/nightOwlDemoAppSlice';

// Navigation Libraries
import {BottomTabScreenProps} from '@react-navigation/bottom-tabs';
import {createStackNavigator, StackScreenProps} from '@react-navigation/stack';
import {HeaderBackButton, HeaderBackButtonProps, HeaderTitle, HeaderTitleProps} from '@react-navigation/elements';
import {CompositeScreenProps} from '@react-navigation/native';

// Transport context
import BluetoothContextContainer from '../../../containers/BluetoothContextContainer';
import LocalWebRTCContextContainer from '../../../containers/LocalWebRTCContextContainer';

// Language context
import {LanguageContext} from '@goe/night-owl-localization';
import {ITranslations} from '../../../../../translations';

// Components
import BluetoothSearchIndicator from '../../../components/Indicators/BluetoothSearchIndicator';
import LocalWebRTCSearchIndicator from '../../../components/Indicators/LocalWebRTCSearchIndicator';
import Base64ArchiveTestComponent from '../../../../../components/TestComponents/Base64ArchiveTestComponent';

// Screens
import TransportDemoHomeScreen from '../../TransportDemo/TransportDemoHomeScreen';
import TransportDemoChargerScreen from '../../TransportDemo/TransportDemoChargerScreen';
import TransportDemoSettingsScreen from '../../TransportDemo/TransportDemoSettingsScreen/TransportDemoSettingsScreen';

// Types
import {NightOwlDemoAppTabParamList, TransportDemoStackParamList} from '../../../types/navigation';
import {ILanguageContext} from '@goe/night-owl-localization';
import {IDispatch} from '../../../../../../App';

interface IDispatchProps {
	moveBackFromSelectedCharger: () => void;
}

type INavigationProps = CompositeScreenProps<
	BottomTabScreenProps<NightOwlDemoAppTabParamList, 'TransportDemo'>,
	StackScreenProps<TransportDemoStackParamList>
>;

type IProps = IDispatchProps & INavigationProps;

const Stack = createStackNavigator<TransportDemoStackParamList>();

const TransportDemoRootUnconnectedScreen: React.FC<IProps> = ({
	route,
	navigation,
	moveBackFromSelectedCharger,
}: IProps): React.ReactElement<IProps> => {
	const {language, getTranslation}: ILanguageContext = React.useContext(LanguageContext);

	return (
		<View style={[styles.background, styles.container]} accessibilityLabel="DemoAppTransportRootScreen">
			<BluetoothContextContainer>
				<LocalWebRTCContextContainer preferredIPType={route.params.preferredIPType}>
					<BluetoothSearchIndicator />
					<LocalWebRTCSearchIndicator />
					{/** Temporary button */}
					<TouchableOpacity
						style={styles.settingsScreenButton}
						onPress={() => navigation.navigate('SettingsScreen')}
						accessibilityLabel="DemoApp_TransportSegment_NavigateToSettingsScreenButton"
					>
						<Text accessibilityLabel="DemoApp_TransportSegment_NavigateToSettingsScreenButton_Text">
							{getTranslation<ITranslations>('settingsScreen', language)}
						</Text>
					</TouchableOpacity>
					<Stack.Navigator>
						<Stack.Screen name={'HomeScreen'} component={TransportDemoHomeScreen} options={{headerShown: false}} />
						<Stack.Screen
							name="ChargerScreen"
							component={TransportDemoChargerScreen}
							options={({navigation: chargerScreenNavigation}) => ({
								headerLeft: (props: HeaderBackButtonProps) => (
									<HeaderBackButton
										{...props}
										onPress={() => {
											chargerScreenNavigation.goBack();
											moveBackFromSelectedCharger();
										}}
										accessibilityLabel="DemoApp_TransportSegment_ChargerScreen_NavigateBackButton"
										label={getTranslation<ITranslations>('homeScreen', language)}
										truncatedLabel={getTranslation<ITranslations>('back', language)}
									/>
								),
								headerTitle: (props: HeaderTitleProps) => (
									<HeaderTitle {...props} accessibilityLabel="DemoApp_TransportSegment_ChargerScreen_Title_Text">
										{getTranslation<ITranslations>('chargerScreen', language)}
									</HeaderTitle>
								),
							})}
						/>
						<Stack.Screen
							name="SettingsScreen"
							component={TransportDemoSettingsScreen}
							options={({navigation: settingsScreenNavigation}) => ({
								headerLeft: (props: HeaderBackButtonProps) => (
									<HeaderBackButton
										{...props}
										onPress={() => settingsScreenNavigation.goBack()}
										accessibilityLabel="DemoApp_TransportSegment_SettingsScreen_NavigateBackButton"
										label={getTranslation<ITranslations>('back', language)}
									/>
								),
								headerTitle: (props: HeaderTitleProps) => (
									<HeaderTitle {...props} accessibilityLabel="DemoApp_TransportSegment_SettingsScreen_Title_Text">
										{getTranslation<ITranslations>('settingsScreen', language)}
									</HeaderTitle>
								),
							})}
						/>
					</Stack.Navigator>
					{/* For tests purposes only */}
					<Base64ArchiveTestComponent isEnabled={config.isDevelop} />
				</LocalWebRTCContextContainer>
			</BluetoothContextContainer>
		</View>
	);
};

const mapDispatchToProps = (dispatch: IDispatch) => ({
	moveBackFromSelectedCharger: () => dispatch(moveBackFromSelectedChargerAction()),
});

export const TransportDemoRootScreen = connect(undefined, mapDispatchToProps)(TransportDemoRootUnconnectedScreen);

const styles = StyleSheet.create({
	background: {
		backgroundColor: '#FFF',
	},
	container: {
		height: '100%',
	},
	settingsScreenButton: {
		backgroundColor: 'yellow',
		width: 50,
		height: 50,
	},
});
