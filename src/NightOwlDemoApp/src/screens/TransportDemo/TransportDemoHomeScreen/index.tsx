import React from 'react';
import {TransportDemoHomeScreen} from './TransportDemoHomeScreen';
import {BluetoothContext} from '@goe/night-owl-transport';
import {LocalWebRTCContext} from '@goe/night-owl-transport';
import {TransportDemoHomeScreenNavigationProps} from '../../../types/navigation';

interface IProps {
	navigation: TransportDemoHomeScreenNavigationProps;
}

const HomeContainer: React.FC<IProps> = ({navigation}: IProps): React.ReactElement<IProps> => {
	const {startBluetoothScan, stopBluetoothScan} = React.useContext(BluetoothContext);
	const {startLocalWebRTCScan, stopLocalWebRTCScan} = React.useContext(LocalWebRTCContext);

	return (
		<TransportDemoHomeScreen
			navigation={navigation}
			stopBluetoothScan={stopBluetoothScan}
			startBluetoothScan={startBluetoothScan}
			startLocalWebRTCScan={startLocalWebRTCScan}
			stopLocalWebRTCScan={stopLocalWebRTCScan}
		/>
	);
};

export default HomeContainer;
