// React
import React from 'react';
import {ScrollView} from 'react-native';

import ChargerDeviceList from '../../../components/ChargerDeviceList';
import {TransportDemoHomeScreenNavigationProps} from '../../../types/navigation';

type Props = {
	navigation: TransportDemoHomeScreenNavigationProps;
};

interface IHomeProps {
	// Bluetooth
	startBluetoothScan: () => void;
	stopBluetoothScan: () => void;

	// WebRTC
	startLocalWebRTCScan: () => void;
	stopLocalWebRTCScan: () => void;
}

interface IHomeState {}

export class TransportDemoHomeScreen extends React.Component<IHomeProps & Props, IHomeState> {
	navigationFocusListener: (() => void) | undefined;
	navigationBlurListener: (() => void) | undefined;

	constructor(props: any) {
		super(props);
	}

	componentDidMount() {
		this.navigationFocusListener = this.props.navigation.addListener('focus', () => {
			this.props.startBluetoothScan();
			this.props.startLocalWebRTCScan();
		});
		this.navigationBlurListener = this.props.navigation.addListener('blur', () => {
			this.props.stopBluetoothScan();
			this.props.stopLocalWebRTCScan();
		});
	}

	componentWillUnmount() {
		// Removing both listeners
		if (this.navigationFocusListener) {
			this.navigationFocusListener();
		}

		if (this.navigationBlurListener) {
			this.navigationBlurListener();
		}
	}

	render() {
		return (
			<ScrollView accessibilityLabel={'DemoApp_TransportSegment_HomeScreen'}>
				<ChargerDeviceList navigation={this.props.navigation} />
			</ScrollView>
		);
	}
}
