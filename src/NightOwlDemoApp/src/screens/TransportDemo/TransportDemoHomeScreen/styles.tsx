import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingTop: 22,
	},
	pingButtonWiFi: {
		width: 100,
		height: 100,
		backgroundColor: 'orange',
	},
	pingButtonBT: {
		width: 100,
		height: 100,
		backgroundColor: 'lightseagreen',
	},
	deviceItem: {
		backgroundColor: 'lightseagreen',
		padding: 10,
	},
	selectedDeviceItem: {
		backgroundColor: 'orange',
		padding: 10,
	},
	item: {
		padding: 10,
		fontSize: 18,
		height: 44,
	},
	logo: {
		width: 128,
		height: 128,
	},
});
