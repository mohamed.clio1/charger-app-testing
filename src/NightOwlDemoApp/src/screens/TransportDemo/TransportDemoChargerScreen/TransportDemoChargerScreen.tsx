import React from 'react';
import {connect} from 'react-redux';
import {Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';

// Components
import MessageConsoleModal from '../../../components/MessageConsoleModal/MessageConsoleModal';

// Resources
import ChargerLogo from '../../../assets/charger/logo.png';

import {selectedChargerSelector} from '../../../redux/selectors/chargerSelectors';

import {IChargerDevice} from '@goe/night-owl-transport';

// Context
import {LanguageContext} from '@goe/night-owl-localization';

// Utils
import {getIsLocalWebRTCAvailable} from '@goe/night-owl-transport';

// Constants
import {AvailableTransports} from '@goe/night-owl-transport';

// Types
import {
	BluetoothServiceName,
	IBluetoothServiceAction,
	ILocalWebRTCServiceAction,
	IServiceAnswer,
	LocalWebRTCChannelLabel,
	LocalWebRTCServiceName,
} from '@goe/night-owl-transport';
import {ILanguageContext} from '@goe/night-owl-localization';
import {IServiceAnswerConsoleMessage} from '../../../components/MessageConsoleModal/MessageConsoleModal';
import {IRootState} from '../../../../../../App';
import {ITranslations} from '../../../../../translations';

// Styles
import {styles} from './styles';

type IProps = {
	selectedChargerDevice: IChargerDevice | null;
	makeLocalWebRTCRequest: <T extends LocalWebRTCChannelLabel, U, V extends LocalWebRTCServiceName>(
		localWebRTCServiceAction: ILocalWebRTCServiceAction<T, U, V>,
	) => Promise<void>;
	localWebRTCServiceAnswer: null | IServiceAnswer;
	makeBluetoothRequest: <T extends BluetoothServiceName, U>(
		bluetoothAction: IBluetoothServiceAction<T, U>,
	) => Promise<void>;
	bluetoothServiceAnswer: null | IServiceAnswer;
	clearLocalWebRTCAnswerConsole: () => void;
	clearBluetoothAnswerConsole: () => void;
};

let isComponentMounted: boolean = false;

// Screen name for accessibility label
const screenName: string = 'DemoApp_TransportSection_ChargerScreen';

const TransportDemoChargerScreen: React.FC<IProps> = ({
	selectedChargerDevice,
	makeLocalWebRTCRequest,
	localWebRTCServiceAnswer,
	makeBluetoothRequest,
	bluetoothServiceAnswer,
	clearLocalWebRTCAnswerConsole,
	clearBluetoothAnswerConsole,
}: IProps): React.ReactElement<IProps> => {
	const {language, getTranslation}: ILanguageContext = React.useContext(LanguageContext);

	const [isServiceAnswerMessagesConsoleVisible, setIsServiceAnswerMessagesConsoleVisible] =
		React.useState<boolean>(false);
	const [serviceAnswerMessageList, setServiceAnswerMessageList] = React.useState<null | IServiceAnswerConsoleMessage[]>(
		null,
	);

	React.useEffect(() => {
		isComponentMounted = true;

		return () => {
			isComponentMounted = false;
		};
	}, []);

	React.useEffect(() => {
		updateAnswerMessageList(localWebRTCServiceAnswer);

		/* eslint-disable-next-line react-hooks/exhaustive-deps */
	}, [localWebRTCServiceAnswer]);

	React.useEffect(() => {
		updateAnswerMessageList(bluetoothServiceAnswer);
		/* eslint-disable-next-line react-hooks/exhaustive-deps */
	}, [bluetoothServiceAnswer]);

	const callLocalWebRTCPingService = (): void => {
		setIsServiceAnswerMessagesConsoleVisible(true);
		makeLocalWebRTCRequest<'LocalWebRTC', undefined, 'PingService'>({
			channelLabel: 'LocalWebRTC',
			payload: undefined,
			serviceName: 'PingService',
		});
	};

	const callLocalWebRTCMetricsService = (): void => {
		setIsServiceAnswerMessagesConsoleVisible(true);
		makeLocalWebRTCRequest<'LocalWebRTC', undefined, 'MetricsService'>({
			channelLabel: 'LocalWebRTC',
			payload: undefined,
			serviceName: 'MetricsService',
		});
	};

	const callLocalWebRTCGetStateService = (): void => {
		setIsServiceAnswerMessagesConsoleVisible(true);
		makeLocalWebRTCRequest<'LocalWebRTC', undefined, 'GetStateService'>({
			channelLabel: 'LocalWebRTC',
			payload: undefined,
			serviceName: 'GetStateService',
		});
	};

	const callLocalWebRTCSetStateChargingOnService = (): void => {
		setIsServiceAnswerMessagesConsoleVisible(true);
		makeLocalWebRTCRequest<'LocalWebRTC', boolean, 'SetStateService'>({
			channelLabel: 'LocalWebRTC',
			payload: true,
			serviceName: 'SetStateService',
		});
	};

	const callLocalWebRTCSetStateChargingOffService = (): void => {
		setIsServiceAnswerMessagesConsoleVisible(true);
		makeLocalWebRTCRequest<'LocalWebRTC', boolean, 'SetStateService'>({
			channelLabel: 'LocalWebRTC',
			payload: false,
			serviceName: 'SetStateService',
		});
	};

	const callBluetoothPingService = (): void => {
		setIsServiceAnswerMessagesConsoleVisible(true);
		makeBluetoothRequest<'PingServiceBluetooth', undefined>({
			serviceName: 'PingServiceBluetooth',
			payload: undefined,
		});
	};

	const callBluetoothMetricsService = (): void => {
		setIsServiceAnswerMessagesConsoleVisible(true);
		makeBluetoothRequest<'MetricsServiceBluetooth', undefined>({
			serviceName: 'MetricsServiceBluetooth',
			payload: undefined,
		});
	};

	const callBluetoothGetStateService = (): void => {
		setIsServiceAnswerMessagesConsoleVisible(true);
		makeBluetoothRequest<'GetStateServiceBluetooth', undefined>({
			serviceName: 'GetStateServiceBluetooth',
			payload: undefined,
		});
	};

	const callBluetoothSetStateChargingOnService = (): void => {
		setIsServiceAnswerMessagesConsoleVisible(true);
		makeBluetoothRequest<'SetStateServiceBluetooth', boolean>({
			serviceName: 'SetStateServiceBluetooth',
			payload: true,
		});
	};

	const callBluetoothSetStateChargingOffService = (): void => {
		setIsServiceAnswerMessagesConsoleVisible(true);
		makeBluetoothRequest<'SetStateServiceBluetooth', boolean>({
			serviceName: 'SetStateServiceBluetooth',
			payload: false,
		});
	};

	const updateAnswerMessageList = (serviceAnswer: IServiceAnswer | null): void => {
		if (serviceAnswer === null) {
			setServiceAnswerMessageList(null);
			return;
		}

		const existingMessageList: IServiceAnswerConsoleMessage[] = serviceAnswerMessageList
			? [...serviceAnswerMessageList]
			: [];

		let newMessageList: IServiceAnswerConsoleMessage[] = [];

		const indexForExistingMessageOfSameTypeAsNewAsnswer: number = existingMessageList.findIndex(
			(existingMessage: IServiceAnswerConsoleMessage): boolean => {
				return existingMessage.serviceType === serviceAnswer.serviceName;
			},
		);

		if (indexForExistingMessageOfSameTypeAsNewAsnswer === -1) {
			const newMessage: IServiceAnswerConsoleMessage = {
				serviceType: serviceAnswer.serviceName,
				messageList: [
					{
						message: getServiceAnswerOrError(serviceAnswer),
						timeStamp: serviceAnswer.timeStamp,
					},
				],
			};

			newMessageList = [...existingMessageList, newMessage];
		} else {
			const existingMessageOfSameTypeAsNewAnswer: IServiceAnswerConsoleMessage =
				existingMessageList[indexForExistingMessageOfSameTypeAsNewAsnswer];

			existingMessageOfSameTypeAsNewAnswer.messageList.push({
				message: getServiceAnswerOrError(serviceAnswer),
				timeStamp: serviceAnswer.timeStamp,
			});
			existingMessageList.splice(
				indexForExistingMessageOfSameTypeAsNewAsnswer,
				1,
				existingMessageOfSameTypeAsNewAnswer,
			);
			newMessageList = existingMessageList;
		}

		if (isComponentMounted) {
			setServiceAnswerMessageList(newMessageList);
		}
	};

	const closeServiceAnswerMessagesConsole = (): void => {
		setIsServiceAnswerMessagesConsoleVisible(false);
		clearLocalWebRTCAnswerConsole();
		clearBluetoothAnswerConsole();
	};

	return (
		<View accessibilityLabel={screenName} style={styles.container}>
			<ScrollView>
				<Image source={ChargerLogo} style={styles.image} />
				<Text accessibilityLabel={`${screenName}_Title_Text`}>
					{getTranslation<ITranslations>('chargerScreenTitle', language)}
				</Text>
				{selectedChargerDevice?.availableTransports.includes(AvailableTransports.Bluetooth) && (
					<>
						<TouchableOpacity
							accessible={true}
							accessibilityLabel={`${screenName}_Bluetooth_Ping_Button`}
							style={styles.pingButtonBT}
							onPress={callBluetoothPingService}
						>
							<Text accessibilityLabel={`${screenName}_Bluetooth_Ping_Button_Text`}>
								{getTranslation<ITranslations>('bluetoothPing', language)}
							</Text>
						</TouchableOpacity>
						<TouchableOpacity
							accessible={true}
							accessibilityLabel={`${screenName}_Bluetooth_MetricsService_Button`}
							style={styles.metricsButtonBT}
							onPress={callBluetoothMetricsService}
						>
							<Text accessibilityLabel={`${screenName}_Bluetooth_MetricsService_Button_Text`}>
								{getTranslation<ITranslations>('bluetoothMetricsService', language)}
							</Text>
						</TouchableOpacity>
						<TouchableOpacity
							accessible={true}
							accessibilityLabel={`${screenName}_Bluetooth_StartChargingService_Button`}
							style={styles.chargeOnButtonBT}
							onPress={callBluetoothSetStateChargingOnService}
						>
							<Text accessibilityLabel={`${screenName}_Bluetooth_StartChargingService_Button_Text`}>
								{getTranslation<ITranslations>('bluetoothStartChargingService', language)}
							</Text>
						</TouchableOpacity>
						<TouchableOpacity
							accessible={true}
							accessibilityLabel={`${screenName}_Bluetooth_StopChargingService_Button`}
							style={styles.chargeOffButtonBT}
							onPress={callBluetoothSetStateChargingOffService}
						>
							<Text accessibilityLabel={`${screenName}_Bluetooth_StopChargingService_Button_Text`}>
								{getTranslation<ITranslations>('bluetoothStopChargingService', language)}
							</Text>
						</TouchableOpacity>
						<TouchableOpacity
							accessible={true}
							accessibilityLabel={`${screenName}_Bluetooth_GetChargingStateService_Button`}
							style={styles.chargerStateButtonBT}
							onPress={callBluetoothGetStateService}
						>
							<Text accessibilityLabel={`${screenName}_Bluetooth_GetChargingStateService_Button_Text`}>
								{getTranslation<ITranslations>('bluetoothGetChargingStateService', language)}
							</Text>
						</TouchableOpacity>
					</>
				)}

				{getIsLocalWebRTCAvailable(selectedChargerDevice) && (
					<>
						<TouchableOpacity
							accessible={true}
							accessibilityLabel={`${screenName}_LocalWebRTC_Ping_Button`}
							style={styles.pingButtonWiFi}
							onPress={callLocalWebRTCPingService}
						>
							<Text accessibilityLabel={`${screenName}_LocalWebRTC_Ping_Button_Text`}>
								{getTranslation<ITranslations>('wifiPing', language)}
							</Text>
						</TouchableOpacity>
						<TouchableOpacity
							accessible={true}
							accessibilityLabel={`${screenName}_LocalWebRTC_MetricsService_Button`}
							style={styles.metricsButtonWifi}
							onPress={callLocalWebRTCMetricsService}
						>
							<Text accessibilityLabel={`${screenName}_LocalWebRTC_MetricsService_Button_Text`}>
								{getTranslation<ITranslations>('wifiMetricsService', language)}
							</Text>
						</TouchableOpacity>
						<TouchableOpacity
							accessible={true}
							accessibilityLabel={`${screenName}_LocalWebRTC_StartChargingService_Button`}
							style={styles.chargeOnButtonWifi}
							onPress={callLocalWebRTCSetStateChargingOnService}
						>
							<Text accessibilityLabel={`${screenName}_LocalWebRTC_StartChargingService_Button_Text`}>
								{getTranslation<ITranslations>('wifiStartChargingService', language)}
							</Text>
						</TouchableOpacity>
						<TouchableOpacity
							accessible={true}
							accessibilityLabel={`${screenName}_LocalWebRTC_StopChargingService_Button`}
							style={styles.chargeOffButtonWifi}
							onPress={callLocalWebRTCSetStateChargingOffService}
						>
							<Text accessibilityLabel={`${screenName}_LocalWebRTC_StopChargingService_Button_Text`}>
								{getTranslation<ITranslations>('wifiStopChargingService', language)}
							</Text>
						</TouchableOpacity>
						<TouchableOpacity
							accessible={true}
							accessibilityLabel={`${screenName}_LocalWebRTC_GetChargingStateService_Button`}
							style={styles.chargerStateButtonWifi}
							onPress={callLocalWebRTCGetStateService}
						>
							<Text accessibilityLabel={`${screenName}_LocalWebRTC_GetChargingStateService_Button_Text`}>
								{getTranslation<ITranslations>('wifiGetChargingStateService', language)}
							</Text>
						</TouchableOpacity>
						{/*)}*/}
					</>
				)}

				{/** Modal for service answer messages display */}
				<MessageConsoleModal
					isVisible={isServiceAnswerMessagesConsoleVisible}
					onClose={closeServiceAnswerMessagesConsole}
					messageList={serviceAnswerMessageList}
				/>
			</ScrollView>
		</View>
	);
};

const mapStateToProps = (state: IRootState) => ({
	selectedChargerDevice: selectedChargerSelector(state),
});

export default connect(mapStateToProps)(TransportDemoChargerScreen);

const getServiceAnswerOrError = (serviceAnswer: IServiceAnswer): string => {
	if (serviceAnswer.serviceName === 'BluetoothError' || serviceAnswer.serviceName === 'LocalWebRTCError') {
		if (serviceAnswer.error) {
			return JSON.stringify(serviceAnswer.error);
		}

		return 'Unknown error';
	}

	return serviceAnswer.answer;
};
