import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
	image: {
		width: 320,
		height: 320,
		marginBottom: 20,
	},
	pingButtonBT: {
		width: 100,
		height: 100,
		backgroundColor: 'orange',
	},
	metricsButtonBT: {
		width: 100,
		height: 100,
		backgroundColor: 'green',
	},
	chargeOnButtonBT: {
		width: 100,
		height: 100,
		backgroundColor: 'blue',
	},
	chargeOffButtonBT: {
		width: 100,
		height: 100,
		backgroundColor: 'yellow',
	},
	chargerStateButtonBT: {
		width: 100,
		height: 100,
		backgroundColor: 'gray',
	},

	pingButtonWiFi: {
		width: 100,
		height: 100,
		backgroundColor: 'orange',
	},
	metricsButtonWifi: {
		width: 100,
		height: 100,
		backgroundColor: 'green',
	},
	chargeOnButtonWifi: {
		width: 100,
		height: 100,
		backgroundColor: 'blue',
	},
	chargeOffButtonWifi: {
		width: 100,
		height: 100,
		backgroundColor: 'yellow',
	},
	chargerStateButtonWifi: {
		width: 100,
		height: 100,
		backgroundColor: 'gray',
	},
});
