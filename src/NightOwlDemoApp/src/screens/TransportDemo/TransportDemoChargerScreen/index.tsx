import React from 'react';
import ChargerScreen from './TransportDemoChargerScreen';
import {BluetoothContext} from '@goe/night-owl-transport';
import {LocalWebRTCContext} from '@goe/night-owl-transport';

const ChargerScreenContainer: React.FC = (): React.ReactElement => {
	const {clearLocalWebRTCAnswerConsole, makeLocalWebRTCRequest, localWebRTCServiceAnswer} =
		React.useContext(LocalWebRTCContext);

	const {makeBluetoothRequest, bluetoothServiceAnswer, clearBluetoothAnswerConsole} =
		React.useContext(BluetoothContext);

	return (
		<ChargerScreen
			makeLocalWebRTCRequest={makeLocalWebRTCRequest}
			localWebRTCServiceAnswer={localWebRTCServiceAnswer}
			makeBluetoothRequest={makeBluetoothRequest}
			bluetoothServiceAnswer={bluetoothServiceAnswer}
			clearLocalWebRTCAnswerConsole={clearLocalWebRTCAnswerConsole}
			clearBluetoothAnswerConsole={clearBluetoothAnswerConsole}
		/>
	);
};

export default ChargerScreenContainer;
