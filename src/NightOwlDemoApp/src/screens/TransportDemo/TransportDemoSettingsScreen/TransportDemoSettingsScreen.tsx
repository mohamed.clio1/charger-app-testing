import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {LanguageContext, LocalizationLanguage} from '@goe/night-owl-localization';

const TransportDemoSettingsScreen: React.FC = (): React.ReactElement<View> => {
	const {language, changeLanguage} = React.useContext(LanguageContext);

	const onLanguageChange = (): void => {
		const nextLanguage: LocalizationLanguage = language === 'en' ? 'de' : 'en';
		changeLanguage(nextLanguage);
	};

	return (
		<View style={styles.container}>
			<TouchableOpacity
				style={styles.button}
				onPress={onLanguageChange}
				accessibilityLabel="DemoApp_TranspostSegment_SettingsScreen_SwitchLanguageButton"
			>
				<Text accessibilityLabel="DemoApp_TranspostSegment_SettingsScreen_SwitchLanguageButton_Text">
					{language === 'en' ? 'Switch to German' : 'Switch to English'}
				</Text>
			</TouchableOpacity>
		</View>
	);
};

export default TransportDemoSettingsScreen;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	button: {
		backgroundColor: 'yellow',
	},
});
