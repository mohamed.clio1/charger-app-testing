import React from 'react';
import {StyleSheet} from 'react-native';
import {StatusBar} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';

// Config
import {config} from '../../../../config';

// Navigation Libraries
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

// Screens
import {TransportDemoRootScreen} from '../TransportDemo/TransportDemoRootScreen/TransportDemoRootScreen';
import {ComponentsDemoRootScreen} from '../ComponentsDemo/ComponentsDemoRootScreen/ComponentsDemoRootScreen';

// Icons
import Ionicons from 'react-native-vector-icons/Ionicons';

// Types
import {NightOwlDemoAppTabParamList} from '../../types/navigation';

const Tab = createBottomTabNavigator<NightOwlDemoAppTabParamList>();

export const NightOwlDemoApp: React.FC = (): React.ReactElement => {
	return (
		<SafeAreaView style={styles.background}>
			<StatusBar barStyle={'dark-content'} />
			<Tab.Navigator>
				<Tab.Screen
					name="ComponentsDemo"
					component={ComponentsDemoRootScreen}
					options={{
						headerShown: false,
						tabBarLabel: 'Component Library',
						tabBarAccessibilityLabel: 'DemoApp_NavigateToComponentsSegmentButton',
						tabBarIcon: ({color, size}) => {
							return <Ionicons name="cube-outline" size={size} color={color} />;
						},
					}}
				/>
				<Tab.Screen
					name="TransportDemo"
					component={TransportDemoRootScreen}
					initialParams={{preferredIPType: config.preferredIPType}}
					options={{
						headerShown: false,
						tabBarLabel: 'Transport demonstration',
						tabBarAccessibilityLabel: 'DemoApp_NavigateToTransportSegmentButton',
						tabBarIcon: ({color, size}) => {
							return <Ionicons name="radio-outline" size={size} color={color} />;
						},
					}}
				/>
			</Tab.Navigator>
		</SafeAreaView>
	);
};

const styles = StyleSheet.create({
	background: {
		backgroundColor: '#FFF',
		flex: 1,
	},
});
