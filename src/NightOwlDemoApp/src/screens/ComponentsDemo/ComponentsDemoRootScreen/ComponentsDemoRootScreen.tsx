import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

export const ComponentsDemoRootScreen: React.FC = (): React.ReactElement => {
	return (
		<View style={styles.container} accessibilityLabel="DemoAppComponentsRootScreen">
			<Text>ComponentsDemoRootScreen</Text>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
});
