import {IConfig} from '../types/config';

export const config: IConfig = {
	// Should be set to false in main (production) branch
	isDevelop: true,
	preferredIPType: 'IPv6',
};
