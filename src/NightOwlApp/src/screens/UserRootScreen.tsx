import React from 'react';

// Navigation
import {createStackNavigator} from '@react-navigation/stack';

// Screens
import {UserScreen} from './UserScreen';
import {AddNewDeviceScreen} from './AddNewDeviceScreen';
import {GuideScreen} from './GuideScreen';
import {SettingsScreen} from './SettingsScreen';
import {EmailAndPasswordScreen} from './EmailAndPasswordScreen';
import {NotificationsScreen} from './NotificationsScreen';
import {ManageDevicesScreen} from './ManageDevicesScreen';
import {ChargerDetailsScreen} from './ChargerDetailsScreen';
import {ManageUsersScreen} from './ManageUsersScreen';
import {AdvancedSpecsScreen} from './AdvancedSpecsScreen';
import {PrivacyScreen} from './PrivacyScreen';
import {LanguageScreen} from './LanguageScreen';
import {SupportScreen} from './SupportScreen';
import {ChatScreen} from './ChatScreen';
import {ReportProblemScreen} from './ReportProblemScreen';
import {FeedbackScreen} from './FeedbackScreen';
import {FAQScreen} from './FAQScreen';

// Types
import {NightOwlUserStackParamList} from '../types/navigation';

const Stack = createStackNavigator<NightOwlUserStackParamList>();

export const UserRootScreen: React.FC = (): React.ReactElement => {
	return (
		<Stack.Navigator initialRouteName="UserScreen" screenOptions={{headerShown: false}}>
			<Stack.Screen name="UserScreen" component={UserScreen} />
			<Stack.Screen name="AddNewDeviceScreen" component={AddNewDeviceScreen} />
			<Stack.Screen name="SettingsScreen" component={SettingsScreen} />
			<Stack.Screen name="EmailAndPasswordScreen" component={EmailAndPasswordScreen} />
			<Stack.Screen name="NotificationsScreen" component={NotificationsScreen} />
			<Stack.Screen name="ManageDevicesScreen" component={ManageDevicesScreen} />
			<Stack.Screen name="ChargerDetailsScreen" component={ChargerDetailsScreen} />
			<Stack.Screen name="ManageUsersScreen" component={ManageUsersScreen} />
			<Stack.Screen name="AdvancedSpecsScreen" component={AdvancedSpecsScreen} />
			<Stack.Screen name="PrivacyScreen" component={PrivacyScreen} />
			<Stack.Screen name="LanguageScreen" component={LanguageScreen} />
			<Stack.Screen name="GuideScreen" component={GuideScreen} />
			<Stack.Screen name="SupportScreen" component={SupportScreen} />
			<Stack.Screen name="ChatScreen" component={ChatScreen} />
			<Stack.Screen name="ReportProblemScreen" component={ReportProblemScreen} />
			<Stack.Screen name="FeedbackScreen" component={FeedbackScreen} />
			<Stack.Screen name="FAQScreen" component={FAQScreen} />
		</Stack.Navigator>
	);
};
