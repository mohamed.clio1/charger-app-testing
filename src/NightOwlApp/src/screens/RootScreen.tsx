import React from 'react';
import {StatusBar, StyleSheet, Text} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';

// Navigation
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

// Icons
import Ionicons from 'react-native-vector-icons/Ionicons';

// Screens
import {HomeRootScreen} from './HomeRootScreen';
import {StatisticsRootScreen} from './StatisticsRootScreen';
import {AchievementsScreen} from './AchievementsScreen';
import {UserRootScreen} from './UserRootScreen';

// Types
import {NightOwlAppTabParamList} from '../types/navigation';
import {RouteProp} from '@react-navigation/core';

const Tab = createBottomTabNavigator<NightOwlAppTabParamList>();

export const NightOwlApp: React.FC = (): React.ReactElement => {
	return (
		<SafeAreaView style={styles.commonBackground}>
			<StatusBar barStyle={'light-content'} />
			<Tab.Navigator
				screenOptions={({route}) => ({
					tabBarIcon: ({focused, size}) => {
						const iconName: string = getTabBarIconName(route, focused);

						return <Ionicons name={iconName} size={size} color={getTabBarIconColor(focused)} />;
					},
					tabBarStyle: {backgroundColor: 'blue'},
					tabBarLabelPosition: 'below-icon',
					headerShown: false,
				})}
				sceneContainerStyle={styles.tabNavigatorBackground}
			>
				<Tab.Screen
					name="RootHomeScreen"
					component={HomeRootScreen}
					options={{
						tabBarLabel: ({focused}) => (
							<Text
								style={[styles.getTabBarLabel, {opacity: getTabBarLabelOpacity(focused)}]}
								accessibilityLabel="NightOwl_NavigateToHomeScreen_BottomBarButton_Text"
							>
								Home
							</Text>
						),
						tabBarAccessibilityLabel: 'NightOwl_NavigateToHomeScreen_BottomBarButton',
					}}
				/>
				<Tab.Screen
					name="RootStatisticsScreen"
					component={StatisticsRootScreen}
					options={{
						tabBarLabel: ({focused}) => (
							<Text
								style={[styles.getTabBarLabel, {opacity: getTabBarLabelOpacity(focused)}]}
								accessibilityLabel="NightOwl_NavigateToStatisticsScreen_BottomBarButton_Text"
							>
								Stats
							</Text>
						),
						tabBarAccessibilityLabel: 'NightOwl_NavigateToStatisticsScreen_BottomBarButton',
					}}
				/>
				<Tab.Screen
					name="RootAchievementsScreen"
					component={AchievementsScreen}
					options={{
						tabBarLabel: ({focused}) => (
							<Text
								style={[styles.getTabBarLabel, {opacity: getTabBarLabelOpacity(focused)}]}
								accessibilityLabel="NightOwl_NavigateToAchievementsScreen_BottomBarButton_Text"
							>
								Achievements
							</Text>
						),
						tabBarAccessibilityLabel: 'NightOwl_NavigateToAchievementsScreen_BottomBarButton',
					}}
				/>
				<Tab.Screen
					name="RootUserScreen"
					component={UserRootScreen}
					options={{
						tabBarLabel: ({focused}) => (
							<Text
								style={[styles.getTabBarLabel, {opacity: getTabBarLabelOpacity(focused)}]}
								accessibilityLabel="NightOwl_NavigateToUserScreen_BottomBarButton_Text"
							>
								User
							</Text>
						),
						tabBarAccessibilityLabel: 'NightOwl_NavigateToUserScreen_BottomBarButton',
					}}
				/>
			</Tab.Navigator>
		</SafeAreaView>
	);
};

const styles = StyleSheet.create({
	commonBackground: {
		flex: 1,
		backgroundColor: 'blue',
	},
	tabNavigatorBackground: {
		backgroundColor: 'transparent',
	},
	getTabBarLabel: {
		color: '#FFF',
		fontSize: 10,
	},
});

const getTabBarIconName = (
	route: RouteProp<NightOwlAppTabParamList, keyof NightOwlAppTabParamList>,
	focused: boolean,
): string => {
	switch (route.name) {
		case 'RootHomeScreen':
			return focused === true ? 'home' : 'home-outline';
		case 'RootStatisticsScreen':
			return focused === true ? 'bar-chart' : 'bar-chart-outline';
		case 'RootAchievementsScreen':
			return focused === true ? 'triangle' : 'triangle-outline';
		case 'RootUserScreen':
			return focused === true ? 'person-circle' : 'person-circle-outline';
	}
};

const getTabBarIconColor = (focused: boolean): string => {
	return focused ? 'rgb(255,255,255)' : 'rgba(255,255,255,0.6)';
};

const getTabBarLabelOpacity = (focused: boolean): number => {
	return focused ? 1 : 0.6;
};
