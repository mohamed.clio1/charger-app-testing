import React from 'react';
import {View} from 'react-native';

// Navigation
import {NativeStackScreenProps} from '@react-navigation/native-stack';

// Components
import {FixmeNavigationHeader} from '../components/FixmeNavigationHeader/FixmeNavigationHeader';

// Types
import {NightOwlHomeStackParamList, NightOwlUserStackParamList} from '../types/navigation';

// Styles
import {screenCommonStyles} from '../styles/screenCommonStyles';

type IHomeNavigationProps = NativeStackScreenProps<NightOwlHomeStackParamList, 'AddNewDeviceScreen'>;
type IUserNavigationProps = NativeStackScreenProps<NightOwlUserStackParamList, 'AddNewDeviceScreen'>;
type IProps = IHomeNavigationProps & IUserNavigationProps;

// Screen name for accessibility label
const screenName: string = 'NightOwl_AddNewDeviceScreen';

export const AddNewDeviceScreen: React.FC<IProps> = ({navigation}: IProps): React.ReactElement<IProps> => {
	return (
		<View
			accessibilityLabel={screenName}
			style={[screenCommonStyles.container, screenCommonStyles.stackCardBackground]}
		>
			<FixmeNavigationHeader
				accessibilityLabel={`${screenName}_NavigationHeader`}
				goBackMethod={() => navigation.goBack()}
				mainTitle="Add charger"
			/>
		</View>
	);
};
