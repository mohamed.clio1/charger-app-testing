import React from 'react';

// Navigation
import {createStackNavigator} from '@react-navigation/stack';

// Screens
import {StatisticsScreen} from './StatisticsScreen';
import {ChargingHistoryScreen} from './ChargingHistoryScreen';

// Types
import {NightOwlStatisticsStackParamList} from '../types/navigation';

const Stack = createStackNavigator<NightOwlStatisticsStackParamList>();

export const StatisticsRootScreen: React.FC = (): React.ReactElement => {
	return (
		<Stack.Navigator initialRouteName="StatisticsScreen" screenOptions={{headerShown: false}}>
			<Stack.Screen name="StatisticsScreen" component={StatisticsScreen} />
			<Stack.Screen name="ChargingHistoryScreen" component={ChargingHistoryScreen} />
		</Stack.Navigator>
	);
};
