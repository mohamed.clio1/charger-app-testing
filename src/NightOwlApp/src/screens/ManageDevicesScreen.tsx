import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';

// Navigation
import {NativeStackScreenProps} from '@react-navigation/native-stack';

// Components
import {FixmeNavigationHeader} from '../components/FixmeNavigationHeader/FixmeNavigationHeader';

// Mocks
import {mockChargerDeviceList} from '../mocks/MockChargerDeviceList/MockChargerDeviceList';

// Types
import {NightOwlHomeStackParamList} from '../types/navigation';
import {NightOwlUserStackParamList} from '../types/navigation';
import {IChargerDevice} from '@goe/night-owl-transport';

// Styles
import {screenCommonStyles} from '../styles/screenCommonStyles';

type IHomeNavigationProps = NativeStackScreenProps<NightOwlHomeStackParamList, 'ManageDevicesScreen'>;
type IUserNavigationProps = NativeStackScreenProps<NightOwlUserStackParamList, 'ManageDevicesScreen'>;
type IProps = IHomeNavigationProps & IUserNavigationProps;

// Screen name for accessibility label
const screenName: string = 'NightOwl_ManageDevicesScreen';

export const ManageDevicesScreen: React.FC<IProps> = ({navigation}: IProps): React.ReactElement<IProps> => {
	return (
		<View
			accessibilityLabel={screenName}
			style={[screenCommonStyles.container, screenCommonStyles.stackCardBackground]}
		>
			<FixmeNavigationHeader
				accessibilityLabel={`${screenName}_NavigationHeader`}
				goBackMethod={() => navigation.goBack()}
				mainTitle="Manage chargers"
			/>
			<TouchableOpacity
				accessibilityLabel={`${screenName}_NavigateToAddNewChargerScreen_Button`}
				style={screenCommonStyles.button}
				onPress={() => navigation.navigate('AddNewDeviceScreen')}
			>
				<Text
					accessibilityLabel={`${screenName}_NavigateToAddNewChargerScreen_Button_Text`}
					style={screenCommonStyles.buttonText}
				>
					Add new device
				</Text>
			</TouchableOpacity>
			{renderChargerDeviceNavigationButtonList(mockChargerDeviceList, navigation)}
		</View>
	);
};

const renderChargerDeviceNavigationButtonList = (
	deviceList: IChargerDevice[],
	navigation: IProps['navigation'],
): React.ReactFragment => {
	return (
		<>
			{deviceList.map(
				(device: IChargerDevice): React.ReactElement<TouchableOpacity> =>
					renderChargerDeviceNavigationButton(device, navigation),
			)}
		</>
	);
};

const renderChargerDeviceNavigationButton = (
	device: IChargerDevice,
	navigation: IProps['navigation'],
): React.ReactElement<TouchableOpacity> => {
	return (
		<TouchableOpacity
			key={device.serialNumber}
			accessibilityLabel={`${screenName}_NavigateToChargerDetailsScreen_${device.serialNumber}_Button`}
			style={screenCommonStyles.button}
			onPress={() => navigation.navigate('ChargerDetailsScreen', {chargerSerialNumber: device.serialNumber})}
		>
			<Text
				accessibilityLabel={`${screenName}_NavigateToChargerDetailsScreen_${device.serialNumber}_Button_Text`}
				style={screenCommonStyles.buttonText}
			>{`Charger ${device.serialNumber}`}</Text>
		</TouchableOpacity>
	);
};
