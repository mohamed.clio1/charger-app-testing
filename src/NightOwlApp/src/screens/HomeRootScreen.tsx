import React from 'react';

// Navigation
import {createStackNavigator} from '@react-navigation/stack';

// Screens
import {HomeScreen} from './HomeScreen';
import {ManageDevicesScreen} from './ManageDevicesScreen';
import {AddNewDeviceScreen} from './AddNewDeviceScreen';
import {ChargerDetailsScreen} from './ChargerDetailsScreen';

// Types
import {NightOwlHomeStackParamList} from '../types/navigation';

const Stack = createStackNavigator<NightOwlHomeStackParamList>();

export const HomeRootScreen: React.FC = (): React.ReactElement => {
	return (
		<Stack.Navigator initialRouteName="HomeScreen" screenOptions={{headerShown: false}}>
			<Stack.Screen name="HomeScreen" component={HomeScreen} />
			<Stack.Screen name="ManageDevicesScreen" component={ManageDevicesScreen} />
			<Stack.Screen name="AddNewDeviceScreen" component={AddNewDeviceScreen} />
			<Stack.Screen name="ChargerDetailsScreen" component={ChargerDetailsScreen} />
		</Stack.Navigator>
	);
};
