import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';

// Navigation
import {NativeStackScreenProps} from '@react-navigation/native-stack';

// Types
import {NightOwlUserStackParamList} from '../types/navigation';

// Styles
import {screenCommonStyles} from '../styles/screenCommonStyles';

type IProps = NativeStackScreenProps<NightOwlUserStackParamList, 'UserScreen'>;

// Screen name for accessibility label
const screenName: string = 'NightOwl_UserScreen';

export const UserScreen: React.FC<IProps> = ({navigation}: IProps): React.ReactElement<IProps> => {
	return (
		<View
			accessibilityLabel={screenName}
			style={[screenCommonStyles.container, screenCommonStyles.stackCardBackground]}
		>
			<Text accessibilityLabel={`${screenName}_Title_Text`} style={screenCommonStyles.title}>
				User
			</Text>
			<TouchableOpacity
				accessibilityLabel={`${screenName}_NavigateToAddNewDevice_Button`}
				style={screenCommonStyles.button}
				onPress={() => navigation.navigate('AddNewDeviceScreen')}
			>
				<Text
					accessibilityLabel={`${screenName}_NavigateToAddNewDevice_Button_Text`}
					style={screenCommonStyles.buttonText}
				>
					Add new device
				</Text>
			</TouchableOpacity>
			<TouchableOpacity
				accessibilityLabel={`${screenName}_NavigateToSettings_Button`}
				style={screenCommonStyles.button}
				onPress={() => navigation.navigate('SettingsScreen')}
			>
				<Text accessibilityLabel={`${screenName}_NavigateToSettings_Button_Text`} style={screenCommonStyles.buttonText}>
					Settings
				</Text>
			</TouchableOpacity>
			<TouchableOpacity
				accessibilityLabel={`${screenName}_NavigateToGuide_Button`}
				style={screenCommonStyles.button}
				onPress={() => navigation.navigate('GuideScreen')}
			>
				<Text accessibilityLabel={`${screenName}_NavigateToGuide_Button_Text`} style={screenCommonStyles.buttonText}>
					Guide
				</Text>
			</TouchableOpacity>
			<TouchableOpacity
				accessibilityLabel={`${screenName}_NavigateToSupport_Button`}
				style={screenCommonStyles.button}
				onPress={() => navigation.navigate('SupportScreen')}
			>
				<Text accessibilityLabel={`${screenName}_NavigateToSupport_Button_Text`} style={screenCommonStyles.buttonText}>
					Support
				</Text>
			</TouchableOpacity>
		</View>
	);
};
