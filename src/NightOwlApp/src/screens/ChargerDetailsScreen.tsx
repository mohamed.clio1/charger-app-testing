import React from 'react';
import {Text, View} from 'react-native';

// Navigation
import {NativeStackScreenProps} from '@react-navigation/native-stack';

// Components
import {FixmeNavigationHeader} from '../components/FixmeNavigationHeader/FixmeNavigationHeader';

// Styles
import {screenCommonStyles} from '../styles/screenCommonStyles';

// Types
import {NightOwlHomeStackParamList, NightOwlUserStackParamList} from '../types/navigation';

type IHomeNavigationProps = NativeStackScreenProps<NightOwlHomeStackParamList, 'ChargerDetailsScreen'>;
type IUserNavigationProps = NativeStackScreenProps<NightOwlUserStackParamList, 'ChargerDetailsScreen'>;
type IProps = IHomeNavigationProps & IUserNavigationProps;

// Screen name for accessibility label
const screenName: string = 'NightOwl_ChargerDetailsScreen';

export const ChargerDetailsScreen: React.FC<IProps> = ({navigation, route}: IProps): React.ReactElement<IProps> => {
	return (
		<View
			accessibilityLabel={screenName}
			style={[screenCommonStyles.container, screenCommonStyles.stackCardBackground]}
		>
			<FixmeNavigationHeader
				accessibilityLabel={`${screenName}_NavigationHeader`}
				goBackMethod={() => navigation.goBack()}
				mainTitle="Details"
			/>
			<Text
				accessibilityLabel={`${screenName}_DeviceName_Text`}
				style={screenCommonStyles.title}
			>{`Charger ${route.params.chargerSerialNumber}`}</Text>
		</View>
	);
};
