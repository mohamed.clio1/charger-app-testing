import React from 'react';
import {Text, View} from 'react-native';

// Styles
import {screenCommonStyles} from '../styles/screenCommonStyles';

// Screen name for accessibility label
const screenName: string = 'NightOwl_AchievementsScreen';

export const AchievementsScreen: React.FC = (): React.ReactElement => {
	return (
		<View accessibilityLabel={screenName} style={screenCommonStyles.container}>
			<Text accessibilityLabel={`${screenName}_Title_Text`} style={screenCommonStyles.title}>
				Achievements
			</Text>
		</View>
	);
};
