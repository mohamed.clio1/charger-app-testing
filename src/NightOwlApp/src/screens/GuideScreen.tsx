import React from 'react';
import {View} from 'react-native';

// Navigation
import {NativeStackScreenProps} from '@react-navigation/native-stack';

// Components
import {FixmeNavigationHeader} from '../components/FixmeNavigationHeader/FixmeNavigationHeader';

// Types
import {NightOwlUserStackParamList} from '../types/navigation';

// Styles
import {screenCommonStyles} from '../styles/screenCommonStyles';

type IProps = NativeStackScreenProps<NightOwlUserStackParamList, 'GuideScreen'>;

// Screen name for accessibility label
const screenName: string = 'NightOwl_UserScreen';

export const GuideScreen: React.FC<IProps> = ({navigation}: IProps): React.ReactElement<IProps> => {
	return (
		<View
			accessibilityLabel={screenName}
			style={[screenCommonStyles.container, screenCommonStyles.stackCardBackground]}
		>
			<FixmeNavigationHeader
				accessibilityLabel={`${screenName}_NavigationHeader`}
				goBackMethod={() => navigation.goBack()}
				mainTitle="Guide"
			/>
		</View>
	);
};
