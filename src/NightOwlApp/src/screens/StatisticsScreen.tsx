import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';

// Navigation
import {NativeStackScreenProps} from '@react-navigation/native-stack';

// Types
import {NightOwlStatisticsStackParamList} from '../types/navigation';

// Styles
import {screenCommonStyles} from '../styles/screenCommonStyles';

type IProps = NativeStackScreenProps<NightOwlStatisticsStackParamList, 'StatisticsScreen'>;

// Screen name for accessibility label
const screenName: string = 'NightOwl_StatisticsScreen';

export const StatisticsScreen: React.FC<IProps> = ({navigation}: IProps): React.ReactElement<IProps> => {
	return (
		<View
			accessibilityLabel={screenName}
			style={[screenCommonStyles.container, screenCommonStyles.stackCardBackground]}
		>
			<Text accessibilityLabel={`${screenName}_Title_Text`} style={screenCommonStyles.title}>
				Charging stats
			</Text>
			<TouchableOpacity
				accessibilityLabel={`${screenName}_NavigateToChargingHistoryScreen_Button`}
				style={screenCommonStyles.button}
				onPress={() => navigation.navigate('ChargingHistoryScreen')}
			>
				<Text
					accessibilityLabel={`${screenName}_NavigateToChargingHistoryScreen_Button_Text`}
					style={screenCommonStyles.buttonText}
				>
					Charging history
				</Text>
			</TouchableOpacity>
		</View>
	);
};
