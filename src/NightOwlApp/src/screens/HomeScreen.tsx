import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';

// Navigation
import {NativeStackScreenProps} from '@react-navigation/native-stack';

// Types
import {NightOwlHomeStackParamList} from '../types/navigation';

// Styles
import {screenCommonStyles} from '../styles/screenCommonStyles';

type IProps = NativeStackScreenProps<NightOwlHomeStackParamList, 'HomeScreen'>;

// Screen name for accessibility label
const screenName: string = 'NightOwl_HomeScreen';

export const HomeScreen: React.FC<IProps> = ({navigation}: IProps): React.ReactElement<IProps> => {
	return (
		<View
			style={[screenCommonStyles.container, screenCommonStyles.stackCardBackground]}
			accessibilityLabel={screenName}
		>
			<TouchableOpacity
				accessibilityLabel={`${screenName}_NavigateToManageDevicesScreen_Button`}
				style={screenCommonStyles.button}
				onPress={() => navigation.navigate('ManageDevicesScreen')}
			>
				<Text
					style={screenCommonStyles.buttonText}
					accessibilityLabel={`${screenName}_NavigateToManageDevicesScreen_Button_Text`}
				>
					Manage devices
				</Text>
			</TouchableOpacity>
		</View>
	);
};
