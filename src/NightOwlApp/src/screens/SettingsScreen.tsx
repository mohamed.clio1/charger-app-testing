import React from 'react';
import {ScrollView, Text, TouchableOpacity, View} from 'react-native';

// Navigation
import {NativeStackScreenProps} from '@react-navigation/native-stack';

// Components
import {FixmeNavigationHeader} from '../components/FixmeNavigationHeader/FixmeNavigationHeader';

// Types
import {NightOwlUserStackParamList} from '../types/navigation';

// Styles
import {screenCommonStyles} from '../styles/screenCommonStyles';

type IProps = NativeStackScreenProps<NightOwlUserStackParamList, 'SettingsScreen'>;

// Screen name for accessibility label
const screenName: string = 'NightOwl_SettingsScreen';

export const SettingsScreen: React.FC<IProps> = ({navigation}: IProps): React.ReactElement<IProps> => {
	return (
		<ScrollView accessibilityLabel={`${screenName}_Scrollable`}>
			<View
				accessibilityLabel={screenName}
				style={[screenCommonStyles.container, screenCommonStyles.stackCardBackground]}
			>
				<FixmeNavigationHeader
					accessibilityLabel={`${screenName}_NavigationHeader`}
					goBackMethod={() => navigation.goBack()}
					mainTitle="Settings"
				/>
				<TouchableOpacity
					accessibilityLabel={`${screenName}_NavigateToEmailAndPasswordScreen_Button`}
					style={screenCommonStyles.button}
					onPress={() => navigation.navigate('EmailAndPasswordScreen')}
				>
					<Text
						accessibilityLabel={`${screenName}_NavigateToEmailAndPasswordScreen_Button_Text`}
						style={screenCommonStyles.buttonText}
					>
						E-mail &amp; password
					</Text>
				</TouchableOpacity>
				<TouchableOpacity
					accessibilityLabel={`${screenName}_NavigateToNotificationsScreen_Button`}
					style={screenCommonStyles.button}
					onPress={() => navigation.navigate('NotificationsScreen')}
				>
					<Text
						accessibilityLabel={`${screenName}_NavigateToNotificationsScreen_Button_Text`}
						style={screenCommonStyles.buttonText}
					>
						Notifications
					</Text>
				</TouchableOpacity>
				<TouchableOpacity
					accessibilityLabel={`${screenName}_NavigateToManageDevicesScreen_Button`}
					style={screenCommonStyles.button}
					onPress={() => navigation.navigate('ManageDevicesScreen')}
				>
					<Text
						accessibilityLabel={`${screenName}_NavigateToManageDevicesScreen_Button_Text`}
						style={screenCommonStyles.buttonText}
					>
						Manage devices
					</Text>
				</TouchableOpacity>
				<TouchableOpacity
					accessibilityLabel={`${screenName}_NavigateToManageUsersScreen_Button`}
					style={screenCommonStyles.button}
					onPress={() => navigation.navigate('ManageUsersScreen')}
				>
					<Text
						accessibilityLabel={`${screenName}_NavigateToManageUsersScreen_Button_Text`}
						style={screenCommonStyles.buttonText}
					>
						Manage users
					</Text>
				</TouchableOpacity>
				<TouchableOpacity
					accessibilityLabel={`${screenName}_NavigateToAdvancedSpecsScreen_Button`}
					style={screenCommonStyles.button}
					onPress={() => navigation.navigate('AdvancedSpecsScreen')}
				>
					<Text
						accessibilityLabel={`${screenName}_NavigateToAdvancedSpecsScreen_Button_Text`}
						style={screenCommonStyles.buttonText}
					>
						Advanced specs
					</Text>
				</TouchableOpacity>
				<TouchableOpacity
					accessibilityLabel={`${screenName}_NavigateToPrivacyScreen_Button`}
					style={screenCommonStyles.button}
					onPress={() => navigation.navigate('PrivacyScreen')}
				>
					<Text
						accessibilityLabel={`${screenName}_NavigateToPrivacyScreen_Button_Text`}
						style={screenCommonStyles.buttonText}
					>
						Privacy
					</Text>
				</TouchableOpacity>
				<TouchableOpacity
					accessibilityLabel={`${screenName}_NavigateToLanguageScreen_Button`}
					style={screenCommonStyles.button}
					onPress={() => navigation.navigate('LanguageScreen')}
				>
					<Text
						accessibilityLabel={`${screenName}_NavigateToLanguageScreen_Button_Text`}
						style={screenCommonStyles.buttonText}
					>
						Language
					</Text>
				</TouchableOpacity>
			</View>
		</ScrollView>
	);
};
