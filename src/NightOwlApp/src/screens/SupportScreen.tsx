import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';

// Navigation
import {NativeStackScreenProps} from '@react-navigation/native-stack';

// Components
import {FixmeNavigationHeader} from '../components/FixmeNavigationHeader/FixmeNavigationHeader';

// Types
import {NightOwlUserStackParamList} from '../types/navigation';

// Styles
import {screenCommonStyles} from '../styles/screenCommonStyles';

type IProps = NativeStackScreenProps<NightOwlUserStackParamList, 'SupportScreen'>;

// Screen name for accessibility label
const screenName: string = 'NightOwl_SupportScreen';

export const SupportScreen: React.FC<IProps> = ({navigation}: IProps): React.ReactElement<IProps> => {
	return (
		<View
			accessibilityLabel={screenName}
			style={[screenCommonStyles.container, screenCommonStyles.stackCardBackground]}
		>
			<FixmeNavigationHeader
				accessibilityLabel={`${screenName}_NavigationHeader`}
				goBackMethod={() => navigation.goBack()}
				mainTitle="Support"
			/>
			<TouchableOpacity
				accessibilityLabel={`${screenName}_NavigateToChatScreen_Button`}
				style={screenCommonStyles.button}
				onPress={() => navigation.navigate('ChatScreen')}
			>
				<Text
					accessibilityLabel={`${screenName}_NavigateToChatScreen_Button_Text`}
					style={screenCommonStyles.buttonText}
				>
					Chat with us
				</Text>
			</TouchableOpacity>
			<TouchableOpacity
				accessibilityLabel={`${screenName}_NavigateToReportProblemScreen_Button`}
				style={screenCommonStyles.button}
				onPress={() => navigation.navigate('ReportProblemScreen')}
			>
				<Text
					accessibilityLabel={`${screenName}_NavigateToReportProblemScreen_Button_Text`}
					style={screenCommonStyles.buttonText}
				>
					Report a problem
				</Text>
			</TouchableOpacity>
			<TouchableOpacity
				accessibilityLabel={`${screenName}_NavigateToFeedbackScreen_Button`}
				style={screenCommonStyles.button}
				onPress={() => navigation.navigate('FeedbackScreen')}
			>
				<Text
					accessibilityLabel={`${screenName}_NavigateToFeedbackScreen_Button_Text`}
					style={screenCommonStyles.buttonText}
				>
					Feedback and suggestions
				</Text>
			</TouchableOpacity>
			<TouchableOpacity
				accessibilityLabel={`${screenName}_NavigateToFAQScreen_Button`}
				style={screenCommonStyles.button}
				onPress={() => navigation.navigate('FAQScreen')}
			>
				<Text
					accessibilityLabel={`${screenName}_NavigateToFAQScreen_Button_Text`}
					style={screenCommonStyles.buttonText}
				>
					FAQ
				</Text>
			</TouchableOpacity>
		</View>
	);
};
