// FIXME This component should be moved to UIKit section
import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {HeaderBackButton} from '@react-navigation/elements';

// Styles
import {screenCommonStyles} from '../../styles/screenCommonStyles';

interface IFixmeNavigationHeader {
	mainTitle: string;
	accessibilityLabel: string;
	goBackMethod: () => void;
}

export const FixmeNavigationHeader: React.FC<IFixmeNavigationHeader> = ({
	accessibilityLabel,
	mainTitle,
	goBackMethod,
}): React.ReactElement<IFixmeNavigationHeader> => {
	return (
		<View style={styles.container}>
			<View style={styles.navigateBackContainer}>
				<HeaderBackButton
					onPress={goBackMethod}
					accessibilityLabel={`${accessibilityLabel}_NavigateBack_Button`}
					label={''}
					tintColor="#FFF"
				/>
			</View>
			<View style={styles.titleContainer}>
				<Text accessibilityLabel={`${accessibilityLabel}_Title_Text`} style={screenCommonStyles.title}>
					{mainTitle}
				</Text>
			</View>
			<View style={styles.emptyContainer} />
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flexDirection: 'row',
		width: '100%',
		alignItems: 'center',
	},
	navigateBackContainer: {
		width: '20%',
		paddingLeft: 20,
	},
	navigateBackTextArrow: {
		fontSize: 30,
		color: '#FFF',
	},
	titleContainer: {
		width: '60%',
		alignItems: 'center',
	},
	emptyContainer: {
		width: '20%',
	},
});
