import {StyleSheet} from 'react-native';

export const screenCommonStyles = StyleSheet.create({
	container: {
		flex: 1,
		paddingTop: 20,
		alignItems: 'center',
		backgroundColor: 'transparent',
	},
	stackCardBackground: {
		backgroundColor: 'blue',
	},
	title: {
		fontSize: 23,
		fontWeight: 'bold',
		color: '#FFF',
	},
	button: {
		width: '90%',
		height: 60,
		backgroundColor: 'rgba(255,255,255,0.3)',
		borderWidth: 1,
		borderColor: '#000',
		borderRadius: 10,
		justifyContent: 'center',
		alignItems: 'center',
		marginTop: 10,
	},
	buttonText: {
		fontSize: 20,
		color: '#FFF',
	},
});
