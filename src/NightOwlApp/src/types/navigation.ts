// Routes for NightOwl App
export type NightOwlAppTabParamList = {
	RootHomeScreen: undefined;
	RootStatisticsScreen: undefined;
	RootAchievementsScreen: undefined;
	RootUserScreen: undefined;
};

// Routes for NightOwl App's Home section
export type NightOwlHomeStackParamList = {
	HomeScreen: undefined;
	ManageDevicesScreen: undefined;
	AddNewDeviceScreen: undefined;
	ChargerDetailsScreen: {chargerSerialNumber: string};
};

// Routes for NightOwl App's Statistics section
export type NightOwlStatisticsStackParamList = {
	StatisticsScreen: undefined;
	ChargingHistoryScreen: undefined;
};

// Routes for NightOwl App's User section
export type NightOwlUserStackParamList = {
	UserScreen: undefined;
	AddNewDeviceScreen: undefined;
	SettingsScreen: undefined;
	EmailAndPasswordScreen: undefined;
	NotificationsScreen: undefined;
	ManageDevicesScreen: undefined;
	ChargerDetailsScreen: {chargerSerialNumber: string};
	ManageUsersScreen: undefined;
	AdvancedSpecsScreen: undefined;
	PrivacyScreen: undefined;
	LanguageScreen: undefined;
	GuideScreen: undefined;
	SupportScreen: undefined;
	ChatScreen: undefined;
	ReportProblemScreen: undefined;
	FeedbackScreen: undefined;
	FAQScreen: undefined;
};
