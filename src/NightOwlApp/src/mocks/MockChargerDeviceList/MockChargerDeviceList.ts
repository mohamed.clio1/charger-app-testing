import {IChargerDevice} from '@goe/night-owl-transport';

export const mockChargerDeviceList: IChargerDevice[] = [
	{
		availableTransports: [0],
		ipAddressList: [{type: 'IPv4', ipAddress: '119.110.9', isAvailable: null}],
		serialNumber: 'SerialNumber1',
		isSelected: false,
		isWebRTCConnection: false,
	},
	{
		availableTransports: [1],
		ipAddressList: [{type: 'IPv4', ipAddress: '119.111.9', isAvailable: true}],
		serialNumber: 'SerialNumber2',
		isSelected: false,
		isWebRTCConnection: false,
	},
	{
		availableTransports: [0, 1],
		ipAddressList: [{type: 'IPv4', ipAddress: '119.110.10', isAvailable: false}],
		serialNumber: 'SerialNumber3',
		isSelected: false,
		isWebRTCConnection: false,
	},
];
