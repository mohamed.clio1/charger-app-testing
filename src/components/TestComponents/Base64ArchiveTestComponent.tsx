import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {NightOwlTransportNativeUtils} from '@goe/night-owl-transport';

export interface IProps {
	isEnabled: boolean;
}

// Test Constants
const base64StringBeforeCompression: string = 'CgtQaW5nU2VydmljZRIEUGluZxoGCgR0ZXN0IAU=';
const base64StringBeforeDecompression: string = 'H4sIAAAAAAAAE+PiDsjMSw9OLSrLTE4VYgFxpNi4WEpSi0sUWAGmTQHaHQAAAA==';

const getAccessibilityLabel = (staticString: string, dynamicString: string): string => {
	return `${staticString}${dynamicString ? ' ' + dynamicString : ''}`;
};

const Base64ArchiveTestComponent: React.FC<IProps> = ({isEnabled}: IProps): React.ReactElement<IProps> | null => {
	const [base64CompressedString, setBase64CompressedString] = React.useState<string>('');
	const [base64DecompressedString, setBase64DecompressedString] = React.useState<string>('');

	if (!isEnabled) {
		return null;
	}

	const onCompressButtonPress = async (): Promise<void> => {
		const compressedString: string = await NightOwlTransportNativeUtils.compressBase64String(
			base64StringBeforeCompression,
		);
		setBase64CompressedString(compressedString);
	};

	const onDecompressButtonPress = async (): Promise<void> => {
		const decompressedString: string = await NightOwlTransportNativeUtils.decompressBase64String(
			base64StringBeforeDecompression,
		);
		setBase64DecompressedString(decompressedString);
	};

	return (
		<View style={styles.container}>
			<TouchableOpacity
				accessible={false}
				accessibilityLabel={getAccessibilityLabel(
					'DemoApp_Base64ArchiveTestComponent_Compress_Button',
					base64CompressedString,
				)}
				style={styles.button}
				onPress={onCompressButtonPress}
			/>
			<TouchableOpacity
				accessible={false}
				accessibilityLabel={getAccessibilityLabel(
					'DemoApp_Base64ArchiveTestComponent_Decompress_Button',
					base64DecompressedString,
				)}
				style={styles.button}
				onPress={onDecompressButtonPress}
			/>
		</View>
	);
};

export default Base64ArchiveTestComponent;

const styles = StyleSheet.create({
	container: {
		width: '100%',
		height: 2,
	},
	button: {
		height: 1,
		width: 1,
	},
});
