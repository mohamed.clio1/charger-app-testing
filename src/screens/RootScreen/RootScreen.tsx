import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {StackNavigationProp} from '@react-navigation/stack';
import {RootStackParamList} from '../../types/navigation';

type RootScreenNavigationProps = StackNavigationProp<RootStackParamList, 'RootScreen'>;

interface IProps {
	navigation: RootScreenNavigationProps;
}

export const RootScreen: React.FC<IProps> = ({navigation}: IProps): React.ReactElement<IProps> => {
	return (
		<View style={styles.container} accessibilityLabel="AppRootScreen">
			<TouchableOpacity
				accessibilityLabel="Root_NavigateToNightOwlAppButton"
				style={styles.button}
				onPress={() => navigation.navigate('NightOwlApp')}
			>
				<Text style={styles.buttonText}>Night Owl App</Text>
			</TouchableOpacity>
			<TouchableOpacity
				accessibilityLabel="Root_NavigateToNightOwlDemoAppButton"
				style={styles.button}
				onPress={() => navigation.navigate('NightOwlDemoApp', {screen: 'ComponentsDemo'})}
			>
				<Text style={styles.buttonText}>Demo App</Text>
			</TouchableOpacity>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	button: {
		width: '100%',
		height: 100,
		backgroundColor: 'rgba(0,0,0,0.3)',
		borderWidth: 1,
		borderColor: '#000',
		justifyContent: 'center',
		alignItems: 'center',
	},
	buttonText: {
		color: '#000',
		fontSize: 28,
	},
});
