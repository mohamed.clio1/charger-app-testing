export interface IError {
	code: string;
	name: string;
	message: string;
	comment: string;
}
