/**
 * {@link console.log} which only called in development mode
 * @param message - Message to print
 * @param optionalParams - Other parameters
 */
export const log = (message?: any, ...optionalParams: any[]): void => {
	if (__DEV__) {
		console.log(message, ...optionalParams);
	}
};

/**
 * {@link console.error} which only called in development mode
 * @param message - Message to print
 * @param optionalParams - Other parameters
 */
export const logError = (message?: any, ...optionalParams: any[]): void => {
	if (__DEV__) {
		console.error(message, ...optionalParams);
	}
};
