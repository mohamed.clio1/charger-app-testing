import {nightOwlDemoAppSlice} from 'NightOwlDemoApp';

export const rootReducer = {
	nightOwlDemoApp: nightOwlDemoAppSlice.reducer,
};
