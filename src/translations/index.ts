import de from './de.json';
import en from './en.json';

type ITranslations = typeof de & typeof en;

export {de, en};
export type {ITranslations};
