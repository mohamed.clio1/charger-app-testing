/**
 * Mocking react-native-wertc module
 */
const ScreenCapturePickerView = {};
const RTCPeerConnection = {};
const RTCIceCandidate = {};
const RTCSessionDescription = {};
const RTCView = {};
const MediaStream = {};
const MediaStreamTrack = {};
const mediaDevices = {};
const permissions = {};
const registerGlobals = () => jest.fn();

export {
	ScreenCapturePickerView,
	RTCPeerConnection,
	RTCIceCandidate,
	RTCSessionDescription,
	RTCView,
	MediaStream,
	MediaStreamTrack,
	mediaDevices,
	permissions,
	registerGlobals,
};
