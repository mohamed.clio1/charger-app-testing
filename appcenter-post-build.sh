if [ "$AGENT_JOBSTATUS" == "Succeeded" ]; then
  echo "::: Platform :::"
  echo $APP_PLATFORM
  if [ "$APP_PLATFORM" == "Android" ]; then
    cd e2e/android
    mvn -DskipTests -P prepare-for-upload package

    appcenter test run appium --app "go-e/ChargerAppAndroidWithCertificate" --devices "go-e/android-devices" --app-path $APPCENTER_OUTPUT_DIRECTORY/app-release.apk --test-series "master" --locale "en_US" --build-dir target/upload --token $AUTOMATION_TOKEN --async
  fi
  if [ "$APP_PLATFORM" == "iOS" ]; then
      cd e2e/ios
      mvn -DskipTests -P prepare-for-upload package

      appcenter test run appium --app "go-e/ChargerAppIOSWithCertificate" --devices "go-e/iphones-set" --app-path $APPCENTER_OUTPUT_DIRECTORY/charger.ipa --test-series "master" --locale "en_US" --build-dir target/upload --token $AUTOMATION_TOKEN --async
  fi
fi
