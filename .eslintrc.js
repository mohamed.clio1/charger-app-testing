module.exports = {
	extends: [
		'goe-common-lint', // common lint config
	],

	plugins: ['react-native', '@react-native-community'],

	overrides: [
		{
			files: ['*.{spec,test}.{js,ts,tsx}', '**/__{mocks,tests}__/**/*.{js,ts,tsx}'],
			env: {
				jest: true,
				'jest/globals': true,
			},
			rules: {
				'react-native/no-inline-styles': 0,
			},
		},
	],
	rules: {
		// React Plugin
		// The following rules are made available via `eslint-plugin-react`.

		'react/react-in-jsx-scope': 1,

		// React-Native Plugin
		// The following rules are made available via `eslint-plugin-react-native`

		'react-native/no-inline-styles': 1,
	},
};
