const {defaults: tsjPreset} = require('ts-jest/presets');

module.exports = {
	...tsjPreset,
	preset: 'react-native',
	moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
	setupFilesAfterEnv: ['./jest.setup.js'],
	testMatch: ['**/__tests__/**/*.+(ts|tsx|js)'],
	transform: tsjPreset.transform,
	transformIgnorePatterns: ['/node_modules/?!(react-native-base64)'],
	globals: {
		'ts-jest': {
			babelConfig: true,
		},
	},
	moduleNameMapper: {
		NightOwlUIKit: '<rootDir>/src/NightOwlUIKit',
		NightOwlDemoApp: '<rootDir>/src/NightOwlDemoApp',
		NightOwlApp: '<rootDir>/src/NightOwlApp',
	},
};
