#!/bin/bash

# Regexp to find a real iPhone's id
regularExpression='(== Devices ==|Known Devices:)(.{5,})(iPhone|iPad \()(.{1,7})(\) (\(|\[))([ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-]{25})'

# List of all connected devices and XCode simulators
deviceList=$( xcrun xctrace list devices );

if
    # Applying the regexp to the device and simulator list
    [[ $deviceList =~ $regularExpression ]];
then
    if
        # Checking whether the 6th match group where the id should reside is not empty
        [ ! -z ${BASH_REMATCH[7]} ];
    then
        iPhone_id=${BASH_REMATCH[7]};
        echo "Found the connected iPhone's id:" $iPhone_id;
        # Navigating to the folder where Appium's WebDriverAgent project located
        cd /Applications/Appium.app/Contents/Resources/app/node_modules/appium/node_modules/appium-webdriveragent;
        # Building and installing WebDriverAgent on the connected iPhone
        xcodebuild -project WebDriverAgent.xcodeproj -scheme WebDriverAgentRunner -destination id=$iPhone_id -allowProvisioningUpdates test;
    else
        echo "Error: iPhone's id not found"
    fi
else
    echo 'Error: no connected iPhone found'
fi
