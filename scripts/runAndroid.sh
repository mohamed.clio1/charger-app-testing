#!/bin/bash

if
    [ -z $ANDROID_HOME ];
then
    # Assuming that by default bash shell used
    filerc='.bash_profile'

    zshrc=~/.zshrc
    zshenv=~/.zshenv
    zprofile=~/.zprofile
    zlogin=~/.zlogin

    if
        # But if there are zsh config files present, assuming that default shell is zsh
        [ -f $zshrc ] || [ -f $zshenv ] || [ -f $zprofilc ] || [ -f $zzloginc ]
    then
        filerc='.zshrc'
    fi

    echo '---==== Setting environment variables ===---'
    echo 'export ANDROID_HOME=$HOME/Library/Android/sdk' >> ~/$filerc
    echo 'export PATH=$PATH:$ANDROID_HOME/emulator' >> ~/$filerc
    echo 'export PATH=$PATH:$ANDROID_HOME/tools' >> ~/$filerc
    echo 'export PATH=$PATH:$ANDROID_HOME/tools/bin' >> ~/$filerc
    echo 'export PATH=$PATH:$ANDROID_HOME/platform-tools' >> ~/$filerc
    echo '---==== Environment variables ===---'
    echo '!!!!! RESTART TERMINAL AND RUN THE SAME SCRIPT AGAIN !!!!!'
else
    echo 'Android environment variables set, proceeding'
    # Changing applicationId in ./android/app/build.gradle to make sure app runs locally
    sed -i '' 's/applicationId "co.goe.chargernext2"/applicationId "com.charger"/g' ./android/app/build.gradle
    react-native run-android --variant=debug
fi
