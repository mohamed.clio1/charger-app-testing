#!/bin/bash

if
	[ -d ./node_modules ]
then
	rm -rf ./node_modules/
fi

if
	[ -f ./yarn.lock ]
then
	rm yarn.lock
fi

if
	[ -d ./ios/Pods ]
then
	rm -rf ./ios/Pods/
fi

if
	[ -f ./ios/Podfile.lock ]
then
	rm ./ios/Podfile.lock
fi

yarn && cd ./ios && pod install 
