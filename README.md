# Charger (NightOwl)

## Installation

In order to install the app in your Mac, you should first gain access to a private npm registry, which contains a few corporate libraries.

1. Make sure you have a Personal Access Token for GitLab. In case you don't have it, create it as instructed here: [https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html). Save it, you will need it for automatic authentication in the GitLab group while installing corporate dependecies.

2. Add the Personal Access Token to the list of your shell environment variables as `GITLAB_AUTH_TOKEN`.

If you use `bash` shell, open `~/.bash_profile` file and add the following line:

```shellscript
export GITLAB_AUTH_TOKEN=<your_access_token_for_gitLab>
```

If you use `zsh` shell, open Terminal and execute the following command:

```shellscript
echo 'export GITLAB_AUTH_TOKEN=<your_access_token_for_gitLab>' >> ~/.zshenv
```

In both cases, replace `<your_access_token_for_gitLab>` with your Personal Access Token.

3. You may now install dependencies:

```shellscript
yarn
```

4. If you have updated any libraries, you need reinstall them so that they get registered in `yarn.lock` file. This file is needed for App Center: it will only use `yarn` for installing dependencies if it detects `yarn.lock`. Otherwise App Center will use `npm`. To effectively update dependencies and `yarn.lock`, use the following command:

```shellscript
yarn clean
```
