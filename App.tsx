// Common Libraries
import 'react-native-gesture-handler';
import React from 'react';
import {StatusBar} from 'react-native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import 'react-native-get-random-values';

// Navigation Libraries
import {
	DefaultTheme,
	NavigationContainer,
	NavigationContainerRefWithCurrent,
	NavigationState,
	Theme,
	useNavigationContainerRef,
} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {HeaderBackButton, HeaderBackButtonProps} from '@react-navigation/elements';

// Redux
import {configureStore} from '@reduxjs/toolkit';
import {Middleware} from 'redux';
import {Provider} from 'react-redux';
import {createLogger} from 'redux-logger';
import {rootReducer} from './src/redux/rootReducer/rootReducer';

// Localization
import {LanguageContext, LanguageProvider, configureLocalization} from '@goe/night-owl-localization';
import {de, en, ITranslations} from './src/translations';

// Screens
import {NightOwlDemoApp} from 'NightOwlDemoApp';
import {NightOwlApp} from 'NightOwlApp';
import {RootScreen} from './src/screens/RootScreen/RootScreen';

// Types
import {RootStackParamList} from './src/types/navigation';

// Setting default language
configureLocalization(en, de);

const middlewares: Middleware[] = [];

if (__DEV__) {
	const createDebugger = require('redux-flipper').default;
	middlewares.push(createDebugger({resolveCyclic: true}));
	middlewares.push(createLogger());
}

const store = configureStore({
	reducer: rootReducer,
	middleware: getDefaultMiddleware => getDefaultMiddleware().concat(...middlewares),
});

const Stack = createStackNavigator<RootStackParamList>();

const LocalizedApp: React.FC = (): React.ReactElement => (
	<SafeAreaProvider>
		<LanguageProvider>
			<App />
		</LanguageProvider>
	</SafeAreaProvider>
);

// White
const navigationThemeDemoBackground: string = 'rgb(242,242,242)';
// Blue
const navigationThemeNightOwlAppBackground: string = 'rgb(0,0,255)';

const navigationThemeDemo: Theme = {
	...DefaultTheme,
	colors: {
		...DefaultTheme.colors,
		background: navigationThemeDemoBackground,
	},
};

const navigationThemeNightOwlApp: Theme = {
	...DefaultTheme,
	colors: {
		...DefaultTheme.colors,
		background: navigationThemeNightOwlAppBackground,
	},
};

const App: React.FC = (): React.ReactElement => {
	const {getTranslation, language} = React.useContext(LanguageContext);
	const [theme, setTheme] = React.useState<Theme>(navigationThemeDemo);

	const navigationRef: NavigationContainerRefWithCurrent<RootStackParamList> = useNavigationContainerRef();

	/**
	 * Changes color theme for React Navigation based on routing
	 * If Root or Demo section is displayed, the background is while
	 * If NightOwl app displayed, the background is blue
	 * @param state - Navigation state received from React Navigation's NavigationContainer
	 */
	const onNavigationStateChange = (state: NavigationState | undefined): void => {
		if (state) {
			const isNightOwlAppDisplayed: boolean = state.routes[1]?.name === 'NightOwlApp' ? true : false;

			if (isNightOwlAppDisplayed && theme.colors.background !== navigationThemeNightOwlAppBackground) {
				setTheme(navigationThemeNightOwlApp);
			} else if (!isNightOwlAppDisplayed && theme.colors.background !== navigationThemeDemoBackground) {
				setTheme(navigationThemeDemo);
			}
		}
	};

	return (
		<NavigationContainer theme={theme} onStateChange={onNavigationStateChange} ref={navigationRef}>
			<Provider store={store}>
				<StatusBar barStyle={'dark-content'} />
				<Stack.Navigator>
					<Stack.Screen
						name="RootScreen"
						component={RootScreen}
						options={{title: getTranslation<ITranslations>('rootScreen', language)}}
					/>
					<Stack.Screen name="NightOwlApp" component={NightOwlApp} options={{headerShown: false}} />
					<Stack.Screen
						name="NightOwlDemoApp"
						component={NightOwlDemoApp}
						options={({navigation: NightOwlDemoAppNavigation}) => ({
							title: 'Demo App',
							headerLeft: (props: HeaderBackButtonProps) => (
								<HeaderBackButton
									{...props}
									onPress={() => NightOwlDemoAppNavigation.goBack()}
									accessibilityLabel="DemoApp_NavigateBackButton"
									label={getTranslation<ITranslations>('rootScreen', language)}
									truncatedLabel={getTranslation<ITranslations>('back', language)}
								/>
							),
						})}
					/>
				</Stack.Navigator>
			</Provider>
		</NavigationContainer>
	);
};

export default LocalizedApp;

export type IRootState = ReturnType<typeof store.getState>;
export type IDispatch = typeof store.dispatch;
